

## Colors

In ECommerce Store you will have different parameters, by which you will want to search products.
One of the most popular parameters are colors and sizes.
In order to show this parameters in filter bar, you have to keep them in separate collection. CRUD for `colors` is
available (CREATE, READ, UPDATE, DELETE).

### Add new color

For adding new color use next set of parameters:

| Property    | Type     | Data type | Generated | Description                                           |
| ----------- | -------- | --------- | --------- | ----------------------------------------------------- |
| `name`      | required | String    | by user   | Name of color (e.g. `blue`, `red`, `light gray` etc.) |
| `cssValue`  | optional | String    | by user   | Hex (#FFFFFF) or rgba(255, 255, 255, 0.5) etc.        |
| `cssStyles` | optional | String    | by user   | String with inline css value if is necessary          |
| `date`      | required | Date      | by system | default `Date.now()`                                  |

You can also pass your custom parameters if necessary for styling and other purposes.

```endpoint
POST /colors
```

#### Example request

```javascript
const newColor = {
  name: "black",
  cssValue: "#ffffff", // This is incorrect, we will edit soon
};

axios
  .post("/colors", newColor)
  .then(newColor => {
    /*Do something with newColor*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4ad9c22012827ecdcc91e",
  "name": "black",
  "cssValue": "#ffffff",
  "date": "2019-10-14T17:17:16.022Z",
  "__v": 0
}
```

#### Example request

```javascript
const newColor = {
  name: "rainbow",
  cssStyles:
    "linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red)",
};

axios
  .post("/colors", newColor)
  .then(newColor => {
    /*Do something with newColor*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4af79f56f6a2748809694",
  "name": "rainbow",
  "cssStyles": "linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red)",
  "date": "2019-10-14T17:25:13.991Z",
  "__v": 0
}
```

### Update color

This endpoint is for editing data of existing color. For finding partiular color for editing it's `_id` (ObjectID) is
used. Pass parameters that you wand to add or edit.

```endpoint
PUT /colors/{id}
```

#### Example request

```javascript
const updatedColor = {
  cssValue: "#000000",
};

axios
  .put("/colors/5da4ad9c22012827ecdcc91e", updatedColor)
  .then(updatedColor => {
    /*Do something with updatedColor*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4ad9c22012827ecdcc91e",
  "name": "black",
  "cssValue": "#000000",
  "date": "2019-10-14T17:17:16.022Z",
  "__v": 0
}
```

### Get colors

Get all colors.

```endpoint
GET /colors
```

#### Example request

```javascript
axios
  .get("/colors")
  .then(colors => {
    /*Do something with colors*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5da4ad9c22012827ecdcc91e",
    "name": "black",
    "cssValue": "#000000",
    "date": "2019-10-14T17:17:16.022Z",
    "__v": 0
  },
  {
    "_id": "5da4af79f56f6a2748809694",
    "name": "rainbow",
    "cssStyles": "linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red)",
    "date": "2019-10-14T17:25:13.991Z",
    "__v": 0
  }
]
```

### Delete color

This endpoint is for deleting existing color by `_id`.

```endpoint
DELETE /colors/{id}
```

#### Example request

```javascript
axios
  .delete("/colors/5da4ad9c22012827ecdcc91e")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Color witn name \"black\" is successfully deletes from DB "
}
```

## Sizes

CRUD for `sizes` is available (CREATE, READ, UPDATE, DELETE). `sizes` is one of most popular parameter of product, that
is why we have separate collection for it. If your product do not have such parameter, you can to not use it.

### Add new size

For adding new size use next set of parameters:

| Property | Type | Data type | Generated | Description |
| --- | --- | --- | --- | --- |
| `name` | required | String | by user | Name of size (e.g. `xl`, `xs`, `standart`, `42` etc.) |
| `date` | required | Date | by system | default `Date.now()` |

You can also pass your custom parameters if necessary.

```endpoint
POST /sizes
```

#### Example request

```javascript
const newSize = {
  name: "xxl",
  description: "extra large size for men clothing",
};

axios
  .post("/sizes", newSize)
  .then(newSize => {
    /*Do something with newSize*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4b51cf56f6a2748809695",
  "name": "xxl",
  "description": "extra large size for men clothing",
  "date": "2019-10-14T17:49:16.142Z",
  "__v": 0
}
```

### Update size

This endpoint is for editing data of existing size. For finding particular size for editing it's `_id` (ObjectID) is
used. Pass parameters that you wand to add or edit.

```endpoint
PUT /sizes/{id}
```

#### Example request

```javascript
const updatedSize = {
  description: "extra extra large size for men clothing",
};

axios
  .put("/sizes/5da4b51cf56f6a2748809695", updatedSize)
  .then(updatedSize => {
    /*Do something with updatedSize*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4b51cf56f6a2748809695",
  "name": "xxl",
  "description": "extra extra large size for men clothing",
  "date": "2019-10-14T17:49:16.142Z",
  "__v": 0
}
```

### Get sizes

Get all sizes.

```endpoint
GET /sizes
```

#### Example request

```javascript
axios
  .get("/sizes")
  .then(sizes => {
    /*Do something with sizes*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5d73d6f122b5802b84ffdede",
    "name": "xs",
    "date": "2019-09-07T16:12:33.230Z",
    "__v": 0
  },
  {
    "_id": "5d73d70a22b5802b84ffdedf",
    "name": "xl",
    "value": "extra large",
    "date": "2019-09-07T16:12:58.095Z",
    "__v": 0
  },
  {
    "_id": "5da4b51cf56f6a2748809695",
    "name": "xxl",
    "description": "extra extra large size for men clothing",
    "date": "2019-10-14T17:49:16.142Z",
    "__v": 0
  }
]
```

### Delete size

This endpoint is for deleting existing size by `_id`.

```endpoint
DELETE /sizes/{id}
```

#### Example request

```javascript
axios
  .delete("/sizes/5d73d6f122b5802b84ffdede")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Size witn name \"xs\" is successfully deletes from DB "
}
```

## Other filters

Besides of colors and sizes your products can have other parameters for filtering. For example: weight, RAM, ROM,
batteryCapacity, brand, manufacturer and many others. Collection "filters" exists for such purposes. You can add there
different objects, which will have parameter "type" to distinguish different filter types. CRUD for filters are
available.

### Add new filter

For adding new filter use next set of parameters:

| operty | Type     | Data type | Generated | Description                                           |
| ------ | -------- | --------- | --------- | ----------------------------------------------------- |
| `type` | required | String    | by user   | e.g. `weight`, `brand`, `manufacturer`, `ram` etc     |
| `name` | required | String    | by user   | Name of filter within it's type ("12mb", "250g" etc.) |
| `date` | required | Date      | by system | default `Date.now()`                                  |

You can also pass your custom parameters if necessary.

```endpoint
POST /filters
```

#### Example request

```javascript
const newFilter = {
  type: "weight",
  name: "100g",
};

axios
  .post("/filters", newFilter)
  .then(newFilter => {
    /*Do something with newFilter*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4b98495987c2364a67b40",
  "type": "weight",
  "name": "100g",
  "date": "2019-10-14T18:08:04.861Z",
  "__v": 0
}
```

### Update filter

This endpoint is for editing data of existing filter by `_id` (ObjectID). Pass parameters that you wand to add or edit.

```endpoint
PUT /filters/{id}
```

#### Example request

```javascript
const updatedFilter = {
  name: "150g",
};

axios
  .put("/filters/5da4b98495987c2364a67b40", updatedFilter)
  .then(updatedFilter => {
    /*Do something with updatedFilter*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da4b98495987c2364a67b40",
  "type": "weight",
  "name": "150g",
  "date": "2019-10-14T18:08:04.861Z",
  "__v": 0
}
```

### Get filters

Get all filters.

```endpoint
GET /filters
```

#### Example request

```javascript
axios
  .get("/filters")
  .then(filters => {
    /*Do something with filters*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5da4b98495987c2364a67b40",
    "type": "weight",
    "name": "150g",
    "date": "2019-10-14T18:08:04.861Z",
    "__v": 0
  },
  {
    "_id": "5da4b9a095987c2364a67b41",
    "type": "weight",
    "name": "250g",
    "date": "2019-10-14T18:08:32.459Z",
    "__v": 0
  },
  {
    "_id": "5da4b9d695987c2364a67b42",
    "type": "brand",
    "name": "adidas",
    "date": "2019-10-14T18:09:26.903Z",
    "__v": 0
  },
  {
    "_id": "5da4b9dd95987c2364a67b43",
    "type": "brand",
    "name": "puma",
    "date": "2019-10-14T18:09:33.813Z",
    "__v": 0
  },
  {
    "_id": "5da4b9ec95987c2364a67b44",
    "type": "weight",
    "name": "1000g",
    "date": "2019-10-14T18:09:48.167Z",
    "__v": 0
  }
]
```

### Get filters by type

Get filters of particular type.

```endpoint
GET /filters/{type}
```

#### Example request

```javascript
axios
  .get("/filters/brand")
  .then(filters => {
    /*Do something with filters*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5da4b9d695987c2364a67b42",
    "type": "brand",
    "name": "adidas",
    "date": "2019-10-14T18:09:26.903Z",
    "__v": 0
  },
  {
    "_id": "5da4b9dd95987c2364a67b43",
    "type": "brand",
    "name": "puma",
    "date": "2019-10-14T18:09:33.813Z",
    "__v": 0
  }
]
```

### Delete filter

This endpoint is for deleting existing filter by `_id`.

```endpoint
DELETE /filters/{id}
```

#### Example request

```javascript
axios
  .delete("/filters/5da4b98495987c2364a67b40")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Filter witn type \"weight\" and name \"150g\" is successfully deletes from DB "
}
```

## Slider

If you have Slider on the main page of site, you can use collection "slides" for managing your slider content. Slider
can show information about products / categories / customers / other information. CRUD for slides are available.

### Add new slide

For adding new slide use next set of parameters:

| Property      | Type     | Data type | Generated | Description                                    |
| ------------- | -------- | --------- | --------- | ---------------------------------------------- |
| `customId`    | required | String    | by user   | Any unique string                              |
| `title`       | optional | String    | by user   | Title that you want to show on slide           |
| `imageUrl`    | required | String    | by user   | Path to image that have to be shown on a slide |
| `description` | optional | String    | by user   | Some text if you want to show it on slide      |
| `htmlContent` | optional | String    | by user   | Some html if you want to show it on slide      |
| `product`     | optional | ObjectId  | by user   | If your slide about specific product           |
| `category`    | optional | ObjectId  | by user   | If your slide about specific category          |
| `customer`    | optional | ObjectId  | by user   | If your slide about specific customer          |
| `date`        | required | Date      | by system | default `Date.now()`                           |

If your slide has referense to particular product / category / customer, you can pass ObjectId of that product/ category
/customer to appropriate field. This slide object will be populated with appropriate product / category / customer
detailes according to saved ObjectId.
You can also pass your custom parameters if necessary.

```endpoint
POST /slides
```

#### Example request

```javascript
const newSlide = {
  customId: "promotion-women-clothing",
  imageUrl: "img/slider/001.png",
  title: "Buy 2 dresses and get a third for free!",
  description: "Do not miss our hot offer. Promotion ends 10/30/2019",
  category: "5d99f68e419d040eec0f722c",
};

axios
  .post("/slides", newSlide)
  .then(newSlide => {
    /*Do something with newSlide*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da7632172b193119c3c8659",
  "customId": "promotion-women-clothing",
  "imageUrl": "img/slider/001.png",
  "title": "Buy 2 dresses and get a third for free!",
  "description": "Do not miss our hot offer. Promotion ends 10/30/2019",
  "category": {
    "_id": "5d99f68e419d040eec0f722c",
    "id": "women",
    "name": "women",
    "parentId": "null",
    "imgUrl": "img/catalog/women.png",
    "description": "A category, that represents products for women",
    "level": 0,
    "date": "2019-10-06T14:13:34.304Z",
    "__v": 0
  },
  "date": "2019-10-16T18:36:17.833Z",
  "__v": 0
}
```

### Update slide

This endpoint is for editing data of existing slide by `customId`. Pass parameters that you wand to add or edit.

```endpoint
PUT /slides/{customId}
```

#### Example request

```javascript
const updatedSlide = {
  imageUrl: "img/slider/002.png",
};

axios
  .put("/slides/promotion-women-clothing", updatedSlide)
  .then(updatedSlide => {
    /*Do something with updatedSlide*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5da7632172b193119c3c8659",
  "customId": "promotion-women-clothing",
  "imageUrl": "img/slider/002.png",
  "title": "Buy 2 dresses and get a third for free!",
  "description": "Do not miss our hot offer. Promotion ends 10/30/2019",
  "category": {
    "_id": "5d99f68e419d040eec0f722c",
    "id": "women",
    "name": "women",
    "parentId": "null",
    "imgUrl": "img/catalog/women.png",
    "description": "A category, that represents products for women",
    "level": 0,
    "date": "2019-10-06T14:13:34.304Z",
    "__v": 0
  },
  "date": "2019-10-16T18:36:17.833Z",
  "__v": 0
}
```

### Get slides

Get all slides.

```endpoint
GET /slides
```

#### Example request

```javascript
axios
  .get("/slides")
  .then(slides => {
    /*Do something with slides*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5da7632172b193119c3c8659",
    "customId": "promotion-women-clothing",
    "imageUrl": "img/slider/002.png",
    "title": "Buy 2 dresses and get a third for free!",
    "description": "Do not miss our hot offer. Promotion ends 10/30/2019",
    "category": {
      "_id": "5d99f68e419d040eec0f722c",
      "id": "women",
      "name": "women",
      "parentId": "null",
      "imgUrl": "img/catalog/women.png",
      "description": "A category, that represents products for women",
      "level": 0,
      "date": "2019-10-06T14:13:34.304Z",
      "__v": 0
    },
    "date": "2019-10-16T18:36:17.833Z",
    "__v": 0
  },
  {
    "_id": "5da767c472b193119c3c865a",
    "customId": "promotion-product-563877",
    "imageUrl": "img/slider/001.png",
    "title": "50% discount for 'test product 5' ",
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73adb9fcad90130470f08f",
      "name": "test product 5",
      "currentPrice": 400,
      "categories": "computers",
      "someOtherFeature": "test5",
      "color": "green",
      "size": "xl",
      "ram": "600",
      "weight": "1800g",
      "itemNo": "563877",
      "__v": 0,
      "date": "2019-10-16T18:56:14.415Z"
    },
    "date": "2019-10-16T18:56:04.674Z",
    "__v": 0
  }
]
```

### Delete slide

This endpoint is for deleting existing slide by `customId`.

```endpoint
DELETE /slides/{customId}
```

#### Example request

```javascript
axios
  .delete("/slides/promotion-women-clothing")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Slide witn customId \"promotion-women-clothing\" is successfully deletes from DB.",
  "deletedSlideInfo": {
    "_id": "5da7632172b193119c3c8659",
    "customId": "promotion-women-clothing",
    "imageUrl": "img/slider/002.png",
    "title": "Buy 2 dresses and get a third for free!",
    "description": "Do not miss our hot offer. Promotion ends 10/30/2019",
    "category": "5d99f68e419d040eec0f722c",
    "date": "2019-10-16T18:36:17.833Z",
    "__v": 0
  }
}
```

## Comments

If you want, you can implement possibility for your customers to leave comments (feedback) about your products.
This is something like reviews. Commenting have to be available only for authenticated customers.

Endpoints for comments allows you to:

- Add comment;
- Update comment;
- Delete comment;
- Get comments;
- Get comments of particular customer;
- Get comments of particular product.

### Add comment

For adding new comment use next set of parameters:

| Property     | Type     | Data type | Generated | Description                                         |
| ------------ | -------- | --------- | --------- | --------------------------------------------------- |
| `customerId` | required | ObjectId  | by system | Not pass this param manually                        |
| `product`    | required | ObjectId  | by user   | ObjectId of product, under which comment was placed |
| `content`    | required | String    | by user   | Comment's content (text or HTML)                    |
| `date`       | required | Date      | by system | default `Date.now()`                                |

You can also pass your custom parameters if necessary.

```endpoint
POST /comments
```

#### Example request

```javascript
const newComment = {
  product: "5da463678cca382250dd7bc7",
  content:
    "I like this product very much! The product is very high quality, I've been using it for a year now.",
};

axios
  .post("/comments", newComment)
  .then(newComment => {
    /*Do something with newComment*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5daf33559202d9245069fd53",
  "product": {
    "enabled": true,
    "imageUrls": [
      "img/products/men/001.png",
      "img/products/men/002.png",
      "img/products/men/003.png",
      "img/products/men/004.png"
    ],
    "quantity": 156,
    "_id": "5da463678cca382250dd7bc7",
    "name": "updted product for testing purposes 222",
    "currentPrice": 100,
    "previousPrice": 250,
    "categories": "men",
    "color": "red",
    "productUrl": "/men",
    "brand": "braaaand",
    "myCustomParam": "some string or json for custom param",
    "itemNo": "291759",
    "date": "2019-10-14T12:00:39.679Z",
    "__v": 0,
    "oneMoreCustomParam": {
      "description": "blablabla",
      "rate": 4.8,
      "likes": 20
    }
  },
  "content": "I like this product very much! The product is very high quality, I've been using it for a year now.",
  "customer": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "__v": 0
}
```

### Update comments

This endpoint is for editing data of existing comment. Pass parameters that you wand to add or edit.

```endpoint
PUT /comments/{id}
```

#### Example request

```javascript
const updatedComment = {
  content: "This product is awful!",
  someCustomParam: "The value of custom param",
};

axios
  .put("/comments/", updatedComment)
  .then(updatedComment => {
    /*Do something with updatedComment*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5daf33559202d9245069fd53",
  "product": {
    "enabled": true,
    "imageUrls": [
      "img/products/men/001.png",
      "img/products/men/002.png",
      "img/products/men/003.png",
      "img/products/men/004.png"
    ],
    "quantity": 156,
    "_id": "5da463678cca382250dd7bc7",
    "name": "updted product for testing purposes 222",
    "currentPrice": 100,
    "previousPrice": 250,
    "categories": "men",
    "color": "red",
    "productUrl": "/men",
    "brand": "braaaand",
    "myCustomParam": "some string or json for custom param",
    "itemNo": "291759",
    "date": "2019-10-14T12:00:39.679Z",
    "__v": 0,
    "oneMoreCustomParam": {
      "description": "blablabla",
      "rate": 4.8,
      "likes": 20
    }
  },
  "content": "This product is awful!",
  "customer": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "__v": 0,
  "someCustomParam": "The value of custom param"
}
```

### Delete comment

This endpoint is for deleting a comment.

```endpoint
DELETE /comments/{id}
```

#### Example request

```javascript
axios
  .delete("/comments/5daf33559202d9245069fd53")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Comment witn id \"5daf33559202d9245069fd53\" is successfully deletes from DB.",
  "deletedCommentInfo": {
    "_id": "5daf33559202d9245069fd53",
    "product": "5da463678cca382250dd7bc7",
    "content": "This product is awful!",
    "customer": "5d99ce196d40fb1b747bc5f5",
    "__v": 0,
    "someCustomParam": "The value of custom param"
  }
}
```

### Get all comments

This endpoint will return all comments.

```endpoint
GET /comments
```

#### Example request

```javascript
axios
  .get("/comments")
  .then(comments => {
    /*Do something with comments*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5daf35595803172a187c1ab8",
    "product": {
      "enabled": true,
      "imageUrls": [
        "img/products/men/001.png",
        "img/products/men/002.png",
        "img/products/men/003.png",
        "img/products/men/004.png"
      ],
      "quantity": 156,
      "_id": "5da463678cca382250dd7bc7",
      "name": "updted product for testing purposes 222",
      "currentPrice": 100,
      "previousPrice": 250,
      "categories": "men",
      "color": "red",
      "productUrl": "/men",
      "brand": "braaaand",
      "myCustomParam": "some string or json for custom param",
      "itemNo": "291759",
      "date": "2019-10-14T12:00:39.679Z",
      "v": 0,
      "oneMoreCustomParam": {
        "description": "blablabla",
        "rate": 4.8,
        "likes": 20
      }
    },
    "content": "Comment about product #1",
    "customer": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "__v": 0
  },
  {
    "_id": "5daf356a5803172a187c1ab9",
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-22T16:59:28.340Z"
    },
    "content": "Comment about product #2",
    "customer": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "__v": 0
  }
]
```

### Get comments of the customer

This endpoint will return all comments of particular customer (user).

```endpoint
GET /comments/customer/{customerId}
```

#### Example request

```javascript
axios
  .get("/comments/customer/5d63a92afc004f2e041179cd")
  .then(comments => {
    /*Do something with comments*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5d90e8fcea6f09306470adb9",
    "customer": {
      "isAdmin": false,
      "enabled": true,
      "_id": "5d63a92afc004f2e041179cd",
      "firstName": "test",
      "lastName": "test",
      "login": "test12",
      "email": "test3@gmail.com",
      "password": "$2a$10$Tfnzth419TINXXQB8pjiYevZCeKdDZmlL1o43k3guhnmiLgMcUkwG",
      "date": "2019-08-26T09:40:58.667Z",
      "__v": 0
    },
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73adb9fcad90130470f08f",
      "name": "test product 5",
      "currentPrice": 400,
      "categories": "computers",
      "someOtherFeature": "test5",
      "color": "green",
      "size": "xl",
      "ram": "600",
      "weight": "1800g",
      "itemNo": "563877",
      "__v": 0,
      "date": "2019-10-22T17:04:07.137Z"
    },
    "content": "222222222222",
    "__v": 0
  },
  {
    "_id": "5d90e9e9ea6f09306470adbb",
    "customer": {
      "isAdmin": false,
      "enabled": true,
      "_id": "5d63a92afc004f2e041179cd",
      "firstName": "test",
      "lastName": "test",
      "login": "test12",
      "email": "test3@gmail.com",
      "password": "$2a$10$Tfnzth419TINXXQB8pjiYevZCeKdDZmlL1o43k3guhnmiLgMcUkwG",
      "date": "2019-08-26T09:40:58.667Z",
      "__v": 0
    },
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73adb9fcad90130470f08f",
      "name": "test product 5",
      "currentPrice": 400,
      "categories": "computers",
      "someOtherFeature": "test5",
      "color": "green",
      "size": "xl",
      "ram": "600",
      "weight": "1800g",
      "itemNo": "563877",
      "__v": 0,
      "date": "2019-10-22T17:04:07.137Z"
    },
    "content": "00000000000000000000",
    "__v": 0
  }
]
```

### Get comments of the product

This endpoint will return all comments of particular product.

```endpoint
GET /comments/product/{productId}
```

#### Example request

```javascript
axios
  .get("/comments/product/5d73adb9fcad90130470f08f")
  .then(comments => {
    /*Do something with comments*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5d90e8fcea6f09306470adb9",
    "customer": {
      "isAdmin": false,
      "enabled": true,
      "_id": "5d63a92afc004f2e041179cd",
      "firstName": "test",
      "lastName": "test",
      "login": "test12",
      "email": "test3@gmail.com",
      "password": "$2a$10$Tfnzth419TINXXQB8pjiYevZCeKdDZmlL1o43k3guhnmiLgMcUkwG",
      "date": "2019-08-26T09:40:58.667Z",
      "__v": 0
    },
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73adb9fcad90130470f08f",
      "name": "test product 5",
      "currentPrice": 400,
      "categories": "computers",
      "someOtherFeature": "test5",
      "color": "green",
      "size": "xl",
      "ram": "600",
      "weight": "1800g",
      "itemNo": "563877",
      "__v": 0,
      "date": "2019-10-22T17:05:21.426Z"
    },
    "content": "<h1>Comments head</h1><p>Comment's content</p>",
    "__v": 0
  },
  {
    "_id": "5d90e9e9ea6f09306470adbb",
    "customer": {
      "isAdmin": false,
      "enabled": true,
      "_id": "5d63a92afc004f2e041179cd",
      "firstName": "test",
      "lastName": "test",
      "login": "test12",
      "email": "test3@gmail.com",
      "password": "$2a$10$Tfnzth419TINXXQB8pjiYevZCeKdDZmlL1o43k3guhnmiLgMcUkwG",
      "date": "2019-08-26T09:40:58.667Z",
      "__v": 0
    },
    "product": {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73adb9fcad90130470f08f",
      "name": "test product 5",
      "currentPrice": 400,
      "categories": "computers",
      "someOtherFeature": "test5",
      "color": "green",
      "size": "xl",
      "ram": "600",
      "weight": "1800g",
      "itemNo": "563877",
      "__v": 0,
      "date": "2019-10-22T17:05:21.426Z"
    },
    "content": "<h1>Comments head</h1><p>Comment's content</p>",
    "__v": 0
  }
]
```

## Subscribers

If you want, you can implement possibility for customers to subscribe to your online shop's news.

Endpoints for subscribers allow you to:

- Add subscriber;
- Update subscriber by id;
- Delete subscriber by email;
- Get subscribers;
- Get a subscriber.

### Add subscriber

For adding new subscriber use next set of parameters:

| Property        | Type     | Data type | Generated | Description                                             |
| --------------- | -------- | --------- | --------- | ------------------------------------------------------- |
| `email`         | required | String    | by user   | Email, which subscribes to news                         |
| `enabled`       | required | Boolean   | by user   | Default - `true`. If unsubscribe - `false`              |
| `letterSubject` | required | String    | by user   | Subject for letter for customer                         |
| `letterHtml`    | required | String    | by user   | String with letter body for sending to customer's email |
| `date`          | required | Date      | by system | default `Date.now()`                                    |

You can also pass your custom parameters if necessary.

All post / put endpoints will send letter ti subscriber. Example in this case:

![Mail to subscriber](images/mail_subscribe.png "Mail to subscriber")

```endpoint
POST /subscribers
```

#### Example request

```javascript
const newSubscriber = {
  email: "saribeg@gmail.com",
  letterSubject: "Test letter (final project)",
  letterHtml:
    "<!DOCTYPE html><html lang='en'> <head> <meta charset='UTF-8' /> <meta name='viewport' content='width=device-width, initial-scale=1.0' /> <meta http-equiv='X-UA-Compatible' content='ie=edge' /> <title>Document</title> <style> td { padding: 20px 50px; background-color: yellow; color: blueviolet; font-size: 20px; } </style> </head> <body> <table> <tr> <td>Test1</td> <td>Test2</td> <td>Test3</td> </tr> <tr> <td>Test1.1</td> <td>Test2.1</td> <td>Test3.1</td> </tr> </table> </body></html>",
};

axios
  .post("/subscribers", newSubscriber)
  .then(newSubscriber => {
    /*Do something with newSubscriber*/;
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/;
  });
```

#### Example response

```json
{
  "subscriber": {
    "enabled": true,
    "_id": "5db076446feb753064b28f85",
    "email": "saribeg@gmail.com",
    "date": "2019-10-23T15:48:20.676Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 837,
    "messageTime": 846,
    "messageSize": 819,
    "response": "250 2.0.0 OK  1571845701 c26sm10885665ljj.45 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<1b406c27-3875-077a-2781-4a1c1507a09b@gmail.com>"
  }
}
```

### Update subscriber by id

This endpoint is for editing data of existing subscriber by ObjectId. Pass parameters that you wand to add or edit. If
you want to unsubscribe customer, you can only change "enabled" to `false`.

```endpoint
PUT /subscribers/{id}
```

#### Example request

```javascript
const updateSubscriber = {
  email: "saribeg@gmail.com",
  enabled: false,
  letterSubject: "Unsubscribe",
  letterHtml: "<p>You are unsubscribed</p>",
};

axios
  .put("/subscribers/5db076446feb753064b28f85", updateSubscriber)
  .then(updateSubscriber => {
    /*Do something with updateSubscriber*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "subscriber": {
    "enabled": false,
    "_id": "5db076446feb753064b28f85",
    "email": "saribeg@gmail.com",
    "date": "2019-10-23T15:48:20.676Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 156,
    "messageTime": 746,
    "messageSize": 292,
    "response": "250 2.0.0 OK 1571846046 b10sm9672143lji.48 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<3cdcd86b-e385-e51c-0928-a345388f237f@gmail.com>"
  }
}
```

### Update subscriber by email

This endpoint is for editing data of existing subscriber by email. Pass parameters that you wand to add or edit. If you
want to unsubscribe customer, you can only change "enabled" to `false`. If you wand switch fron unsubscribed to
subscribed - "enabled" to `true`.

```endpoint
PUT /subscribers/email/{email}
```

#### Example request

```javascript
const updateSubscriber = {
  enabled: true,
  letterSubject: "Welcome back",
  letterHtml: "<p>We are glad to see you!</p>",
};

axios
  .put("/subscribers/email/saribeg@gmail.com", updateSubscriber)
  .then(updateSubscriber => {
    /*Do something with updateSubscriber*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "subscriber": {
    "enabled": true,
    "_id": "5db076446feb753064b28f85",
    "email": "saribeg@gmail.com",
    "date": "2019-10-23T15:48:20.676Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 181,
    "messageTime": 1011,
    "messageSize": 296,
    "response": "250 2.0.0 OK 1571846387 j30sm2748880lfk.50 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<53f0c288-89ee-839f-d1c5-aa75dfd9ae75@gmail.com>"
  }
}
```

### Get all subscriberss

This endpoint will return all subscribers.

```endpoint
GET /subscribers
```

#### Example request

```javascript
axios
  .get("/subscribers")
  .then(subscribers => {
    /*Do something with subscribers*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "enabled": true,
    "_id": "5d75162d0787c50e0cf20d17",
    "email": "saribeg2@gmail.com",
    "date": "2019-09-08T14:54:37.654Z",
    "__v": 0
  },
  {
    "enabled": true,
    "_id": "5d7518d2c14b1c0474b7dbc7",
    "email": "saribeg4@gmail.com",
    "date": "2019-09-08T15:05:54.841Z",
    "__v": 0
  },
  {
    "enabled": true,
    "_id": "5d7518f9c14b1c0474b7dbc8",
    "email": "saribeg5@gmail.com",
    "date": "2019-09-08T15:06:33.790Z",
    "__v": 0
  },
  {
    "enabled": true,
    "_id": "5db076446feb753064b28f85",
    "email": "saribeg@gmail.com",
    "date": "2019-10-23T15:48:20.676Z",
    "__v": 0
  }
]
```

### Get subscriber by email

This endpoint will return one subscriber by email.

```endpoint
GET /subscribers/{email}
```

#### Example request

```javascript
axios
  .get("/subscribers/saribeg@gmail.com")
  .then(subscribers => {
    /*Do something with subscribers*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "_id": "5db076446feb753064b28f85",
  "email": "saribeg@gmail.com",
  "date": "2019-10-23T15:48:20.676Z",
  "__v": 0
}
```

## Links

Your site may contain different links, that redirect to static pages.
For example, different links in footer.
Something like "About us", "Terms and Conditions", "Privacy policy", "Contact us" and others.
In order to be able to controll this links from panel of administrator, you can keep these links in your DB collection.
Next endpoints for links are available:

- Add links;
- Update links;
- Delete links;
- Get links;
- Get specific links.

### Add new links

For adding new link use next set of parameters:

| Property | Type     | Data type | Generated | Description                                    |
| -------- | -------- | --------- | --------- | ---------------------------------------------- |
| `title`  | required | String    | by user   | e.g. "About us", "Documents" etc.              |
| `links`  | required | json      | by user   | Array of objects with links, relevant to title |
| `date`   | required | Date      | by system | default `Date.now()`                           |

Property `links` is array ob object in which every object has next properties:

| Property      | Type     | Data type | Generated | Description                  |
| ------------- | -------- | --------- | --------- | ---------------------------- |
| `description` | required | String    | by user   | The text-value of link       |
| `url`         | required | String    | by user   | Path to static page for link |

You can also pass your custom parameters if necessary.

```endpoint
POST /links
```

#### Example request

```javascript
const newLinks = {
  title: "Documentation",
  links: [
    {
      description: "Privacy Policy",
      url: "/documents/privacy-policy",
    },
    {
      description: "Terms and conditions",
      url: "/documents/terms-and-conditions",
    },
    {
      description: "Payment Rules",
      url: "/documents/payment-rules",
    },
  ],
};

axios
  .post("/links", newLinks)
  .then(newLinks => {
    /*Do something with newLinks*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5db5ac89421f580bec129949",
  "title": "Documentation",
  "links": [
    {
      "_id": "5db5ac89421f580bec12994c",
      "description": "Privacy Policy",
      "url": "/documents/privacy-policy"
    },
    {
      "_id": "5db5ac89421f580bec12994b",
      "description": "Terms and conditions",
      "url": "/documents/terms-and-conditions"
    },
    {
      "_id": "5db5ac89421f580bec12994a",
      "description": "Payment Rules",
      "url": "/documents/payment-rules"
    }
  ],
  "date": "2019-10-27T14:41:13.256Z",
  "__v": 0
}
```

### Update links

This endpoint is for editing data of existing links by `_id` (ObjectID). Pass parameters that you wand to add or edit.

```endpoint
PUT /links/{id}
```

#### Example request

```javascript
const updatedLinks = {
  title: "Documents",
};

axios
  .put("/links/5db5ac89421f580bec129949", updatedLinks)
  .then(updatedLinks => {
    /*Do something with updatedLinks*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5db5ac89421f580bec129949",
  "title": "Documents",
  "links": [
    {
      "_id": "5db5ac89421f580bec12994c",
      "description": "Privacy Policy",
      "url": "/documents/privacy-policy"
    },
    {
      "_id": "5db5ac89421f580bec12994b",
      "description": "Terms and conditions",
      "url": "/documents/terms-and-conditions"
    },
    {
      "_id": "5db5ac89421f580bec12994a",
      "description": "Payment Rules",
      "url": "/documents/payment-rules"
    }
  ],
  "date": "2019-10-27T14:41:13.256Z",
  "__v": 0
}
```

### Get links

Get all links.

```endpoint
GET /links
```

#### Example request

```javascript
axios
  .get("/links")
  .then(links => {
    /*Do something with links*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5db5ac89421f580bec129949",
    "title": "Documents",
    "links": [
      {
        "_id": "5db5ac89421f580bec12994c",
        "description": "Privacy Policy",
        "url": "/documents/privacy-policy"
      },
      {
        "_id": "5db5ac89421f580bec12994b",
        "description": "Terms and conditions",
        "url": "/documents/terms-and-conditions"
      },
      {
        "_id": "5db5ac89421f580bec12994a",
        "description": "Payment Rules",
        "url": "/documents/payment-rules"
      }
    ],
    "date": "2019-10-27T14:41:13.256Z",
    "__v": 0
  },
  {
    "_id": "5db5ae53659c7f07e4744834",
    "title": "Our Store",
    "links": [
      {
        "_id": "5db5ae53659c7f07e4744837",
        "description": "About Us",
        "url": "/documents/aboutus"
      },
      {
        "_id": "5db5ae53659c7f07e4744836",
        "description": "Careers",
        "url": "/documents/careers"
      },
      {
        "_id": "5db5ae53659c7f07e4744835",
        "description": "Contact Us",
        "url": "/documents/contactus"
      }
    ],
    "date": "2019-10-27T14:48:51.499Z",
    "__v": 0
  }
]
```

### Get specific links

Get links of particular type.

```endpoint
GET /links/{id}
```

#### Example request

```javascript
axios
  .get("/links/5db5ae53659c7f07e4744834")
  .then(links => {
    /*Do something with links*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5db5ae53659c7f07e4744834",
  "title": "Our Store",
  "links": [
    {
      "_id": "5db5ae53659c7f07e4744837",
      "description": "About Us",
      "url": "/documents/aboutus"
    },
    {
      "_id": "5db5ae53659c7f07e4744836",
      "description": "Careers",
      "url": "/documents/careers"
    },
    {
      "_id": "5db5ae53659c7f07e4744835",
      "description": "Contact Us",
      "url": "/documents/contactus"
    }
  ],
  "date": "2019-10-27T14:48:51.499Z",
  "__v": 0
}
```

### Delete links

This endpoint is for deleting existing links by `_id`.

```endpoint
DELETE /links/{id}
```

#### Example request

```javascript
axios
  .delete("/links//5db5ae53659c7f07e4744834")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Links witn title \"Our Store\" is successfully deletes from DB. "
}
```

## Pages

You can have different pages on your site. Such as "About Us", "Contact Us", "Careers", "Terms and Conditions" ans so
on.
In order to be able to controll content of these pages from panel of administrator, you can keep content in your DB
collection.

Next endpoints for pages are available:

- Add page;
- Update page;
- Delete page;
- Get page.

### Add new page

For adding new page use next set of parameters:

| Property      | Type     | Data type | Generated | Description                                     |
| ------------- | -------- | --------- | --------- | ----------------------------------------------- |
| `customId`    | required | String    | by user   | e.g. "about-us", "contact-us" etc.              |
| `title`       | required | String    | by user   | Some title for your page                        |
| `images`      | optional | json      | by user   | Urls to images for that page (array of strings) |
| `htmlContent` | required | String    | by user   | Your page content in HTML                       |
| `date`        | required | Date      | by system | default `Date.now()`                            |

You can also pass your custom parameters if necessary.

```endpoint
POST /pages
```

#### Example request

```javascript
const newPage = {
  customId: "about-us",
  title: "About Us",
  images: ["/img/pages/aboutus/001.png", "/img/pages/aboutus/002.png"],
  htmlContent: "<p>Our mission.......</p><p>We are progressive.......</p>",
};

axios
  .post("/pages", newPage)
  .then(newPage => {
    /*Do something with newPage*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "images": ["/img/pages/aboutus/001.png", "/img/pages/aboutus/002.png"],
  "_id": "5db5d228a58bbd2c64797db3",
  "customId": "about-us",
  "title": "About Us",
  "htmlContent": "<p>Our mission.......</p><p>We are progressive.......</p>",
  "date": "2019-10-27T17:21:44.477Z",
  "__v": 0
}
```

### Update page

This endpoint is for editing data of existing page by customId. Pass parameters that you wand to add or edit.

```endpoint
PUT /pages/{customId}
```

#### Example request

```javascript
const updatedPage = {
  htmlContent:
    "<ul><li>Lorem ipsum 1</li><li>Lorem ipsum 2</li><li>Lorem ipsum 3</li></ul>",
};

axios
  .put("/pages/about-us", updatedPage)
  .then(updatedPage => {
    /*Do something with updatedPage*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "images": ["/img/pages/aboutus/001.png", "/img/pages/aboutus/002.png"],
  "_id": "5db5d228a58bbd2c64797db3",
  "customId": "about-us",
  "title": "About Us",
  "htmlContent": "<ul><li>Lorem ipsum 1</li><li>Lorem ipsum 2</li><li>Lorem ipsum 3</li></ul>",
  "date": "2019-10-27T17:21:44.477Z",
  "__v": 0
}
```

### Get page

```endpoint
GET /pages/{customId}
```

#### Example request

```javascript
axios
  .get("/pages/about-us")
  .then(page => {
    /*Do something with page*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "images": ["/img/pages/aboutus/001.png", "/img/pages/aboutus/002.png"],
  "_id": "5db5d228a58bbd2c64797db3",
  "customId": "about-us",
  "title": "About Us",
  "htmlContent": "<ul><li>Lorem ipsum 1</li><li>Lorem ipsum 2</li><li>Lorem ipsum 3</li></ul>",
  "date": "2019-10-27T17:21:44.477Z",
  "__v": 0
}
```

### Delete page

This endpoint is for deleting existing page.

```endpoint
DELETE /pages/{customId}
```

#### Example request

```javascript
axios
  .delete("/pages//about-us")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Page with customId \"about-us\" is successfully deletes from DB.",
  "deletedPageInfo": {
    "images": ["/img/pages/aboutus/001.png", "/img/pages/aboutus/002.png"],
    "_id": "5db5d228a58bbd2c64797db3",
    "customId": "about-us",
    "title": "About Us",
    "htmlContent": "<ul><li>Lorem ipsum 1</li><li>Lorem ipsum 2</li><li>Lorem ipsum 3</li></ul>",
    "date": "2019-10-27T17:21:44.477Z",
    "__v": 0
  }
}
```

## Shipping methods

What is shipping methods? You can find information here:

- [SHIPPING METHODS](https://planetexpress.com/shipping-methods/)
- [Shipping Methods](https://support.bigcommerce.com/s/article/Shipping-Methods)
- [Shipping Methods define where orders can be shipped and what the costs are.](https://docs.commercetools.com/http-api-projects-shippingMethods)

CRUD for `shipping methods` is available (CREATE, READ, UPDATE, DELETE).

You can configure up to 2-3 shipping methods in your store (without fanaticism). Shipping methods we use in checkout
process.

### Add new shipping method

For adding new shipping method use next set of parameters:

| Property               | Type     | Data type | Generated | Description                               |
| ---------------------- | -------- | --------- | --------- | ----------------------------------------- |
| `customId`             | required | String    | by user   | Some unique string                        |
| `name`                 | required | String    | by user   | -                                         |
| `description`          | optional | String    | by user   | -                                         |
| `locations`            | required | json      | by user   | Countries to ship to                      |
| `enabled`              | required | Boolean   | by user   | Default = false                           |
| `default`              | required | Boolean   | by user   | Default = false                           |
| `imageUrl`             | required | String    | by user   | -                                         |
| `costValue`            | required | Number    | by user   | How much it costs                         |
| `costType`             | required | String    | by user   | "fix" or "percent"                        |
| `freeShippingOrderSum` | optional | Number    | by user   | Sum of order from which shipping is free  |
| `currency`             | optional | String    | by user   | `USD`, `EUR` depending on shipping method |
| `period`               | optional | String    | by user   | e.g. "3 - 5 days"                         |
| `date`                 | required | Date      | by system | default `Date.now()`                      |

You can also pass your custom parameters if necessary.

```endpoint
POST /shipping-methods
```

#### Example request

```javascript
const newShippingMethod = {
  customId: "shipping-method-1",
  name: "Shipping Method #1",
  locations: [{ country: "UKR" }, { country: "RUS" }],
  costValue: 50,
  costType: "fix",
  freeShippingOrderSum: 300,
  period: "1 day",
  currency: "UAH",
};

axios
  .post("/shipping-methods", newShippingMethod)
  .then(newShippingMethod => {
    /*Do something with newShippingMethod*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "costValue": 50,
  "costType": "fix",
  "_id": "5dbee039246d7b1ba0c779a6",
  "customId": "shipping-method-1",
  "name": "Shipping Method #1",
  "locations": [
    {
      "country": "UKR"
    },
    {
      "country": "RUS"
    }
  ],
  "freeShippingOrderSum": 300,
  "period": "1 day",
  "currency": "UAH",
  "date": "2019-11-03T14:12:09.139Z",
  "__v": 0
}
```

### Update shipping method

This endpoint is for editing data of existing shipping method. For finding particular shipping method for editing it's
customId is used. Pass parameters that you wand to add or edit.

```endpoint
PUT /shipping-methods/{customId}
```

#### Example request

```javascript
const updatedShippingMethod = {
  costValue: 100,
  freeShippingOrderSum: 500,
  period: "2 days",
};

axios
  .put("/shipping-methods/shipping-method-1", updatedShippingMethod)
  .then(updatedShippingMethod => {
    /*Do something with updatedShippingMethod*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "costValue": 100,
  "costType": "fix",
  "_id": "5dbee039246d7b1ba0c779a6",
  "customId": "shipping-method-1",
  "name": "Shipping Method #1",
  "locations": [
    {
      "country": "UKR"
    },
    {
      "country": "RUS"
    }
  ],
  "freeShippingOrderSum": 500,
  "period": "2 days",
  "currency": "UAH",
  "date": "2019-11-03T14:12:09.139Z",
  "__v": 0
}
```

### Get shipping methods

Get all shipping methods.

```endpoint
GET /shipping-methods
```

#### Example request

```javascript
axios
  .get("/shipping-methods")
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "enabled": true,
    "default": false,
    "costValue": 50,
    "costType": "fix",
    "_id": "5db97b344f42d81a14e574fe",
    "customId": "cost50",
    "name": "cost50",
    "freeShippingOrderSum": 1000,
    "date": "2019-10-30T11:59:48.145Z",
    "__v": 0
  },
  {
    "enabled": true,
    "default": false,
    "costValue": 100,
    "costType": "fix",
    "_id": "5dbee039246d7b1ba0c779a6",
    "customId": "shipping-method-1",
    "name": "Shipping Method #1",
    "locations": [
      {
        "country": "UKR"
      },
      {
        "country": "RUS"
      }
    ],
    "freeShippingOrderSum": 500,
    "period": "2 days",
    "currency": "UAH",
    "date": "2019-11-03T14:12:09.139Z",
    "__v": 0
  }
]
```

### Get shipping methods by id

Get shipping method by customId.

```endpoint
GET /shipping-methods/{customId}
```

#### Example request

```javascript
axios
  .get("/shipping-methods/cost50")
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "costValue": 50,
  "costType": "fix",
  "_id": "5db97b344f42d81a14e574fe",
  "customId": "cost50",
  "name": "cost50",
  "freeShippingOrderSum": 1000,
  "date": "2019-10-30T11:59:48.145Z",
  "__v": 0
}
```

### Delete shipping method

This endpoint is for deleting existing shipping method by customId.

```endpoint
DELETE /shipping-methods/{customId}
```

#### Example request

```javascript
axios
  .delete("/shipping-methods/cost50")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Shipping method witn name \"cost50\" is successfully deleted from DB ",
  "deletedDocument": {
    "enabled": true,
    "default": false,
    "costValue": 50,
    "costType": "fix",
    "_id": "5db97b344f42d81a14e574fe",
    "customId": "cost50",
    "name": "cost50",
    "freeShippingOrderSum": 1000,
    "date": "2019-10-30T11:59:48.145Z",
    "__v": 0
  }
}
```

## Payment methods

What is payment methods? You can find information here:

- [Types of payment methods for ecommerce](https://blog.paymentwall.com//guides/types-of-payment-methods-for-ecommerce)
- [Payment Methods](https://www.braintreepayments.com/features/payment-methods)
- [Choosing payment methods](https://www.business.gov.au/finance/payments-and-invoicing/choosing-payment-methods)
- [The Top 20 Payment Gateway Providers](https://www.fundera.com/blog/payment-gateway-providers)

CRUD for `payment methods` is available (CREATE, READ, UPDATE, DELETE).

You can configure up to 2-3 payment methods in your store (without fanaticism). Payment methods we use in checkout
process.

### Add new payment method

For adding new payment method use next set of parameters:

| Property           | Type     | Data type | Generated | Description          |
| ------------------ | -------- | --------- | --------- | -------------------- |
| `customId`         | required | String    | by user   | Some unique string   |
| `name`             | required | String    | by user   | -                    |
| `description`      | optional | String    | by user   | -                    |
| `enabled`          | required | Boolean   | by user   | Default = false      |
| `default`          | required | Boolean   | by user   | Default = false      |
| `imageUrl`         | required | String    | by user   | -                    |
| `paymentProcessor` | optional | any       | by user   |                      |
| `date`             | required | Date      | by system | default `Date.now()` |

You can also pass your custom parameters if necessary.

```endpoint
POST /payment-methods
```

#### Example request

```javascript
const newPaymentMethod = {
  customId: "payment-method-1",
  name: "Payment Method #1",
  paymentProcessor: "Adyen",
};

axios
  .post("/payment-methods", newPaymentMethod)
  .then(newPaymentMethod => {
    /*Do something with newPaymentMethod*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "_id": "5dbefb80e842b61a50611032",
  "customId": "payment-method-1",
  "name": "Payment Method #1",
  "paymentProcessor": "Adyen",
  "date": "2019-11-03T16:08:32.089Z",
  "__v": 0
}
```

### Update payment method

This endpoint is for editing data of existing payment method. For finding particular payment method for editing it's
customId is used. Pass parameters that you wand to add or edit.

```endpoint
PUT /payment-methods/{customId}
```

#### Example request

```javascript
const updatedPaymentMethod = {
  paymentProcessor: "Braintree",
  imageUrl: "/img/paymentIcons/001.png",
};

axios
  .put("/payment-methods/payment-method-1", updatedPaymentMethod)
  .then(updatedPaymentMethod => {
    /*Do something with updatedPaymentMethod*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "_id": "5dbefb80e842b61a50611032",
  "customId": "payment-method-1",
  "name": "Payment Method #1",
  "paymentProcessor": "Braintree",
  "date": "2019-11-03T16:08:32.089Z",
  "__v": 0,
  "imageUrl": "/img/paymentIcons/001.png"
}
```

### Get payment methods

Get all payment methods.

```endpoint
GET /payment-methods
```

#### Example request

```javascript
axios
  .get("/payment-methods")
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "enabled": true,
    "default": false,
    "_id": "5dbefb80e842b61a50611032",
    "customId": "payment-method-1",
    "name": "Payment Method #1",
    "paymentProcessor": "Braintree",
    "date": "2019-11-03T16:08:32.089Z",
    "__v": 0,
    "imageUrl": "/img/paymentIcons/001.png"
  },
  {
    "enabled": true,
    "default": false,
    "_id": "5dbefc4be842b61a50611033",
    "customId": "payment-method-2",
    "name": "Payment Method #2",
    "paymentProcessor": "Adyen",
    "date": "2019-11-03T16:11:55.785Z",
    "__v": 0
  }
]
```

### Get payment methods by id

Get payment method by customId.

```endpoint
GET /payment-methods/{customId}
```

#### Example request

```javascript
axios
  .get("/payment-methods/payment-method-1")
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "default": false,
  "_id": "5dbefb80e842b61a50611032",
  "customId": "payment-method-1",
  "name": "Payment Method #1",
  "paymentProcessor": "Braintree",
  "date": "2019-11-03T16:08:32.089Z",
  "__v": 0,
  "imageUrl": "/img/paymentIcons/001.png"
}
```

### Delete payment method

This endpoint is for deleting existing payment method by customId.

```endpoint
DELETE /payment-methods/{customId}
```

#### Example request

```javascript
axios
  .delete("/payment-methods/payment-method-1")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Payment Method witn name \"payment-method-1\" is successfully deleted from DB ",
  "deletedDocument": {
    "enabled": true,
    "default": false,
    "_id": "5dbefb80e842b61a50611032",
    "customId": "payment-method-1",
    "name": "Payment Method #1",
    "paymentProcessor": "Braintree",
    "date": "2019-11-03T16:08:32.089Z",
    "__v": 0,
    "imageUrl": "/img/paymentIcons/001.png"
  }
}
```

## Partners

You can manage your partners on store.

CRUD for `partners` is available (CREATE, READ, UPDATE, DELETE).

### Add new partner

For adding new partner use next set of parameters:

| Property      | Type     | Data type | Generated | Description          |
| ------------- | -------- | --------- | --------- | -------------------- |
| `customId`    | required | String    | by user   | Some unique string   |
| `name`        | required | String    | by user   | -                    |
| `description` | optional | String    | by user   | -                    |
| `enabled`     | required | Boolean   | by user   | Default = false      |
| `imageUrl`    | required | String    | by user   | -                    |
| `url`         | optional | String    | by user   |                      |
| `date`        | required | Date      | by system | default `Date.now()` |

You can also pass your custom parameters if necessary.

```endpoint
POST /partners
```

#### Example request

```javascript
const newPartner = {
  customId: "adidas",
  name: "Adidas",
  imageUrl: "/img/partners/adidas/001.png",
  url: "https://www.adidas.com/us",
};

axios
  .post("/partners", newPartner)
  .then(newPartner => {
    /*Do something with newPartner*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "_id": "5dbefebfe102601f40c164bb",
  "customId": "adidas",
  "name": "Adidas",
  "imageUrl": "/img/partners/adidas/001.png",
  "url": "https://www.adidas.com/us",
  "date": "2019-11-03T16:22:23.672Z",
  "__v": 0
}
```

### Update partner

This endpoint is for editing data of existing partner. For finding particular partner for editing it's customId is used.
Pass parameters that you wand to add or edit.

```endpoint
PUT /partners/{customId}
```

#### Example request

```javascript
const updatedPartner = {
  imageUrl: "/img/partners/adidas/002.png",
};

axios
  .put("/partners/adidas", updatedPartner)
  .then(updatedPartner => {
    /*Do something with updatedPartner*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "_id": "5dbefebfe102601f40c164bb",
  "customId": "adidas",
  "name": "Adidas",
  "imageUrl": "/img/partners/adidas/002.png",
  "url": "https://www.adidas.com/us",
  "date": "2019-11-03T16:22:23.672Z",
  "__v": 0
}
```

### Get partners

Get all partners.

```endpoint
GET /partners
```

#### Example request

```javascript
axios
  .get("/partners")
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "enabled": true,
    "_id": "5dbefebfe102601f40c164bb",
    "customId": "adidas",
    "name": "Adidas",
    "imageUrl": "/img/partners/adidas/002.png",
    "url": "https://www.adidas.com/us",
    "date": "2019-11-03T16:22:23.672Z",
    "__v": 0
  },
  {
    "enabled": true,
    "_id": "5dbeffc3e102601f40c164bc",
    "customId": "puma",
    "name": "Puma",
    "imageUrl": "/img/partners/puma/001.png",
    "url": "https://ru.puma.com/",
    "date": "2019-11-03T16:26:43.026Z",
    "__v": 0
  }
]
```

### Delete partner

This endpoint is for deleting existing partner by customId.

```endpoint
DELETE /partners/{customId}
```

#### Example request

```javascript
axios
  .delete("/partners/adidas")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Partner witn name \"adidas\" is successfully deleted from DB ",
  "deletedDocument": {
    "enabled": true,
    "_id": "5dbefebfe102601f40c164bb",
    "customId": "adidas",
    "name": "Adidas",
    "imageUrl": "/img/partners/adidas/002.png",
    "url": "https://www.adidas.com/us",
    "date": "2019-11-03T16:22:23.672Z",
    "__v": 0
  }
}
```
