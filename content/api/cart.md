
## Cart

If customer is not logged in, you can save cart in browser's local storage and manipulate with cart's data in similar
way, as in this API, adding to it new products, editing added products' quantity and deleting products from cart.
If customer is authenticated, you can save cart in database. It is comfortable, because if customer will log in from
another device, he will see his cart from database.
If customer has cart in local storage and then he authenticates, you can migrate cart from local storage to DB if
authenticated customer has not such in DB.

Endpoints for cart allows you to:

- Create cart (only 1 instance). Customer can not have more than one cart;
- Update (edit) cart. If cart is not created yet, this will create cart;
- Add product to cart;
- Decrease product quantity in cart;
- Delete cart;
- Delete product from cart;
- Get cart.

In most cases it is enough to use three of them: Add product to cart, Delete product from cart and Get cart. For
example:

![Cart](images/cart.png "Cart")

### Create cart

For creating new cart use next set of parameters:

| Property     | Type     | Data type | Generated | Description                                             |
| ------------ | -------- | --------- | --------- | ------------------------------------------------------- |
| `customerId` | optional | ObjectId  | by system | Not pass this param manually                            |
| `products`   | optional | json      | by user   | Array of objects with info about product and quantities |
| `date`       | required | Date      | by system | default `Date.now()`                                    |

The parameter `products` should have the next structure:

      "products": [
            {
                "product": "5da463678cca382250dd7bc7",
                "cartQuantity": 1
            },
            {
                "product": "5d73ad04fcad90130470f08b",
                "cartQuantity": 3
            },
            {
                "product": "5d73ad2bfcad90130470f08c",
                "cartQuantity": 13
            }
        ]

As you see, `products` is array of objects. Every object contains key `product` and value a particular product's
ObjectId from mongoDB products collection and also key `cartQuantity` with value number (how many of particular products
is added to cart).
In local storage save data about products in cart in similar way and always (every time when customer visit his cart
page) retrieve information about products from DB to have all updates about prices and availibility.

You can also pass your custom parameters if necessary.

```endpoint
POST /cart
```

#### Example request

```javascript
const newCart = {
  products: [
    {
      product: "5da463678cca382250dd7bc7",
      cartQuantity: 1,
    },
  ],
};

axios
  .post("/cart", newCart)
  .then(newCart => {
    /*Do something with newCart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac1d324f3a6f0e0cb19ddd",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 1
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Update cart

This endpoint is for editing data of existing cart. Pass parameters that you wand to add or edit.

```endpoint
PUT /cart
```

#### Example request

```javascript
const updatedCart = {
  products: [
    {
      product: "5da463678cca382250dd7bc7",
      cartQuantity: 2,
    },
    {
      product: "5d73ad04fcad90130470f08b",
      cartQuantity: 3,
    },
  ],
};

axios
  .put("/cart", updatedCart)
  .then(updatedCart => {
    /*Do something with updatedCart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac20058b2cb420e0af4676",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad04fcad90130470f08b",
        "name": "test product",
        "currentPrice": 280,
        "categories": "phones",
        "someOtherFeature": "Test feature strict false 2222222222",
        "color": "black",
        "size": "xl",
        "ram": "5",
        "weight": "200g",
        "itemNo": "243965",
        "__v": 0,
        "date": "2019-10-20T08:51:19.287Z"
      },
      "cartQuantity": 3
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Add product to cart

Add one product to cart. If cart does not exists, it will be created and product will be added to it. If cart already
exists, product will be added to existing arrays of products.

```endpoint
PUT /cart/{productId}
```

#### Example request

```javascript
axios
  .put("/cart/5d73ad2bfcad90130470f08c")
  .then(updatedCart => {
    /*Do something with updatedCart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac20058b2cb420e0af4676",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad04fcad90130470f08b",
        "name": "test product",
        "currentPrice": 280,
        "categories": "phones",
        "someOtherFeature": "Test feature strict false 2222222222",
        "color": "black",
        "size": "xl",
        "ram": "5",
        "weight": "200g",
        "itemNo": "243965",
        "__v": 0,
        "date": "2019-10-20T08:57:51.224Z"
      },
      "cartQuantity": 3
    },
    {
      "_id": "5dac218e8b2cb420e0af4678",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad2bfcad90130470f08c",
        "name": "test product 2",
        "currentPrice": 35,
        "categories": "phones",
        "someOtherFeature": "test2",
        "color": "white",
        "size": "x",
        "ram": "23",
        "weight": "100g",
        "itemNo": "341527",
        "__v": 0,
        "date": "2019-10-20T08:57:51.225Z"
      },
      "cartQuantity": 1
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Decrease product quantity

```endpoint
DELETE /cart/product/{productId}
```

#### Example request

```javascript
axios
  .delete("/cart/5d73ad2bfcad90130470f08c")
  .then(updatedCart => {
    /*Do something with updatedCart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac20058b2cb420e0af4676",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad04fcad90130470f08b",
        "name": "test product",
        "currentPrice": 280,
        "categories": "phones",
        "someOtherFeature": "Test feature strict false 2222222222",
        "color": "black",
        "size": "xl",
        "ram": "5",
        "weight": "200g",
        "itemNo": "243965",
        "__v": 0,
        "date": "2019-10-20T08:57:51.224Z"
      },
      "cartQuantity": 3
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Delete product from cart

This endpoint is for deleting one product from cart.

```endpoint
DELETE /cart/{productId}
```

#### Example request

```javascript
axios
  .delete("/cart/5d73ad04fcad90130470f08b")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac218e8b2cb420e0af4678",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad2bfcad90130470f08c",
        "name": "test product 2",
        "currentPrice": 35,
        "categories": "phones",
        "someOtherFeature": "test2",
        "color": "white",
        "size": "x",
        "ram": "23",
        "weight": "100g",
        "itemNo": "341527",
        "__v": 0,
        "date": "2019-10-20T09:00:22.299Z"
      },
      "cartQuantity": 1
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Get cart

This endpoint will return cart with detail information about customer and all products, added to cart.

```endpoint
GET /cart
```

#### Example request

```javascript
axios
  .get("/cart")
  .then(cart => {
    /*Do something with cart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5dac1d324f3a6f0e0cb19ddc",
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac218e8b2cb420e0af4678",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad2bfcad90130470f08c",
        "name": "test product 2",
        "currentPrice": 35,
        "categories": "phones",
        "someOtherFeature": "test2",
        "color": "white",
        "size": "x",
        "ram": "23",
        "weight": "100g",
        "itemNo": "341527",
        "__v": 0,
        "date": "2019-10-20T09:00:22.299Z"
      },
      "cartQuantity": 1
    }
  ],
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-20T08:39:14.936Z",
  "__v": 0
}
```

### Delete cart

This endpoint is for deleting the whole cart.

```endpoint
DELETE /cart
```

#### Example request

```javascript
axios
  .delete("/cart")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Cart witn id \"5dac1d324f3a6f0e0cb19ddc\" is successfully deletes from DB "
}
```
