## Catalog

In ECommerce Store you will have different products.
Products can be assigned to a particular category.
The set of categories, that have hierarchical bindings is called "Catalog".
The catalog is a collection in which each object has the following form:

```
{
      "_id": "5d6a8385ff10dc0af0c90e5c",
      "id": "men-clothing-suits",
      "name": "suits",
      "parentId": "men-clothing",
      "__v": 0,
      "date": "2019-10-06T13:50:11.859Z"
    }

```

`parentId` - is the id of parent category.
If some category has highest level of hierarchy and has not parent canegory, then the `parentId` should be `'null'` (
String).

Using `parentId` parameter, you can create catalog-tree or some other data-structure for iterating through it in
frontend code to show categories in navigation menu and / or in filter bar etc.

We have endpoints to:

- Add new category into the catalog;
- Edit specific category in the catalog;
- Get catalog;
- Get specific category from catalog;
- Delete specific category from catalog.

### Add new category to Catalog

For adding new category use next set of parameters:

| Property   | Type     | Data type | Generated | Description                                 |
| ---------- | -------- | --------- | --------- | ------------------------------------------- |
| `id`       | required | String    | by user   | Your custom id                              |
| `name`     | required | String    | by user   | Name of category to show on store interface |
| `parentId` | required | String    | by user   | Id of the **parent** category               |
| `date`     | required | Date      | by system | default `Date.now()`                        |

You can also pass your custom parameters.
For example, category imgUrl or some description if you want on store show additional information about category. Even a
level of nesting if it will reduce the complexity of using received data from DB.
**Important**: `id` parameter (your custom id) is non-changable. At first you have to define the id-naming logic for
your categories and then add categories to DB. If something was saved wrong - you can change it in DB directly.

```endpoint
POST /catalog
```

#### Example request

```javascript
// high level category

const newCategory = {
  id: "women",
  name: "women",
  parentId: "null",
  imgUrl: "img/catalog/women.png",
  description: "A category, that represents products for women",
  level: 0,
};

axios
  .post("/catalog", newCategory)
  .then(newCategory => {
    /*Do something with newCategory*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });

// Child category

const newCategory = {
  id: "women-shoes",
  name: "shoes",
  parentId: "women",
  imgUrl: "img/catalog/women-shoes.png",
  description: "A category, that represents shoes for women",
  level: 1,
};

axios
  .post("/catalog", newCategory)
  .then(newCategory => {
    /*Do something with newCategory*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5d99f68e419d040eec0f722c",
  "id": "women",
  "name": "women",
  "parentId": "null",
  "imgUrl": "img/catalog/women.png",
  "description": "A category, that represents products for women",
  "level": 0,
  "date": "2019-10-06T14:13:34.304Z",
  "__v": 0
}
```

```json
{
  "_id": "5d99f77d419d040eec0f722d",
  "id": "women-shoes",
  "name": "shoes",
  "parentId": "women",
  "imgUrl": "img/catalog/women-shoes.png",
  "description": "A category, that represents shoes for women",
  "level": 1,
  "date": "2019-10-06T14:17:33.743Z",
  "__v": 0
}
```

### Update category

This endpoint is for editing data of existing category. For finding partiular category for editing it's `id` is used.
Pass parameters that you wand to add or edit.

```endpoint
PUT /catalog/{id}
```

#### Example request

```javascript
const updatedCategory = {
  name: "SHOES",
  description:
    "A category, that represents beautifull branded shoes for women. A цide range of products.",
};

axios
  .put("/catalog/women-shoes", updatedCategory)
  .then(updatedCategory => {
    /*Do something with updatedCategory*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5d99f77d419d040eec0f722d",
  "id": "women-shoes",
  "name": "SHOES",
  "parentId": "women",
  "imgUrl": "img/catalog/women-shoes.png",
  "description": "A category, that represents beautifull branded shoes for women. A цide range of products.",
  "level": 1,
  "date": "2019-10-06T14:17:33.743Z",
  "__v": 0
}
```

### Get catalog

This endpoint is for geting the whole catalog (all categories).

```endpoint
GET /catalog
```

#### Example request

```javascript
axios
  .get("/catalog")
  .then(catalog => {
    /*Do something with catalog*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "_id": "5d99f68e419d040eec0f722c",
    "id": "women",
    "name": "women",
    "parentId": "null",
    "imgUrl": "img/catalog/women.png",
    "description": "A category, that represents products for women",
    "level": 0,
    "date": "2019-10-06T14:13:34.304Z",
    "__v": 0
  },
  {
    "_id": "5d99f77d419d040eec0f722d",
    "id": "women-shoes",
    "name": "SHOES",
    "parentId": "women",
    "imgUrl": "img/catalog/women-shoes.png",
    "description": "A category, that represents beautifull branded shoes for women. A цide range of products.",
    "level": 1,
    "date": "2019-10-06T14:17:33.743Z",
    "__v": 0
  }
]
```

### Get category

This endpoint is for geting one spesific category by it's custom id.

```endpoint
GET /catalog/{id}
```

#### Example request

```javascript
axios
  .get("/catalog/women")
  .then(category => {
    /*Do something with category*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "_id": "5d99f68e419d040eec0f722c",
  "id": "women",
  "name": "women",
  "parentId": "null",
  "imgUrl": "img/catalog/women.png",
  "description": "A category, that represents products for women",
  "level": 0,
  "date": "2019-10-06T14:13:34.304Z",
  "__v": 0
}
```

### Delete category

This endpoint is for deleting existing category. For finding spesific category for deleting from DB you must use
category's custom id.

Be careful, when deleting category from catalog, as it can have bindings.
For example, you can accidentally delete parent category and as a result all it's child categories (if such exists) will
have incorrect binding in `parentId` as that parent category will already be deleted.
There are not any validations on server for such cases.

```endpoint
DELETE /catalog/{id}
```

#### Example request

```javascript
axios
  .delete("/catalog/women-shoes")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Category witn id \"women-shoes\" is successfully deleted from DB.",
  "deletedCategoryInfo": {
    "_id": "5d99f77d419d040eec0f722d",
    "id": "women-shoes",
    "name": "SHOES",
    "parentId": "women",
    "imgUrl": "img/catalog/women-shoes.png",
    "description": "A category, that represents beautifull branded shoes for women. A цide range of products.",
    "level": 1,
    "date": "2019-10-06T14:17:33.743Z",
    "__v": 0
  }
}
```