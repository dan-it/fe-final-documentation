
## Customers

We have endpoints for:

- Create new customer (register);
- Log in;
- Get customer;
- Update existing customer (edit customer's data);
- Update (change) customers password.

### Create new customer (register)

For creating new customer you have to pass next parameters when making request to the server's appropriate endpoint:

| Property     | Type     | Data type | Generated | Description          |
| ------------ | -------- | --------- | --------- | -------------------- |
| `customerNo` | required | String    | by system | -                    |
| `firstName`  | required | String    | by user   | -                    |
| `lastName`   | required | String    | by user   | -                    |
| `login`      | required | String    | by user   | -                    |
| `email`      | required | String    | by user   | -                    |
| `password`   | required | String    | by user   | -                    |
| `isAdmin`    | required | Boolean   | by system | default `false`      |
| `enabled`    | required | Boolean   | by system | default `true`       |
| `telephone`  | optional | String    | by user   | -                    |
| `birthdate`  | optional | String    | by user   | -                    |
| `gender`     | optional | String    | by user   | -                    |
| `avatarUrl`  | optional | String    | by user   | -                    |
| `date`       | optional | Date      | by system | default `Date.now()` |

You can also add your own custom parameters to save in database if you want. Passing custom parameters can help you with
implementing some custom functionality or logic if necessary.

```endpoint
POST /customers
```

#### Example request

```javascript
const newCustomer = {
  firstName: "Customer",
  lastName: "Newone",
  login: "Customer",
  email: "customer@gmail.com",
  password: "1111111",
  telephone: "+380630000000",
  gender: "male",
  avatarUrl: "img/customers/023648.png",
  isAdmin: true,
};

axios
  .post("/customers", newCustomer)
  .then(savedCustomer => {
    /*Do something with customer*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to customer*/
  });
```

#### Example response

```json
{
  "isAdmin": true,
  "enabled": true,
  "_id": "5d99d2532cd5241c40fc9008",
  "firstName": "Customer",
  "lastName": "Newone",
  "login": "Customer",
  "email": "customer@gmail.com",
  "password": "$2a$10$XTXkPWN3zJufG9S3NATZyeeuoVOwvrzqopfdqYwbUQidCZHp/aTYS",
  "telephone": "+380630000000",
  "gender": "male",
  "avatarUrl": "img/customers/023648.png",
  "customerNo": "27314974",
  "date": "2019-10-06T11:38:59.470Z",
  "__v": 0
}
```

### Login

Log into store. For login process customer can use his email address or login.

Parameters:

| Property       | Type     | Data type | Description    |
| -------------- | -------- | --------- | -------------- |
| `loginOrEmail` | required | String    | email or login |
| `password`     | required | String    | -              |

You will receive back jwt-token and can use it for checking expire date, loggedin status etc. For example, you can save
token in local storage and then use `jwt-decode` module for decoding jwt and checking if it has expiration or not,
logout etc.

```endpoint
POST /customers/login
```

#### Example request

```javascript
const userData = {
  loginOrEmail: "customer@gmail.com",
  password: "1111111",
};

axios
  .post("/customers/login", userData)
  .then(loginResult => {
    /*Do something with jwt-token if login successed*/
  })
  .catch(err => {
    /*Show error to customer, may be incorrect password or something else*/
  });
```

#### Example response

```json
{
  "success": true,
  "token": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkOTljZTE5NmQ0MGZiMWI3NDdiYzVmNSIsImZpcnN0TmFtZSI6IkN1c3RvbWVyIiwibGFzdE5hbWUiOiJOZXdvbmUiLCJpYXQiOjE1NzAzNjU0ODIsImV4cCI6MTU3MDM2OTA4Mn0._arHw0HC2uLsH6Ow0LJEeuaj03S4dB9x--yfp5Kj_r0"
}
```

### Get customer

Get current customer's data. We are talking about a customer who is logged into the system (not any customer by id).
There are different endpoints that require user authorization. If endpoint is available only for authorizied users, you
have to provide appropriate Header for axios request. You can provide some universal function, that will handle it. For
example:

```
import axios from 'axios';

    const setAuthToken = token => {
      if (token) {
        // Apply to every request
        axios.defaults.headers.common['Authorization'] = token;
      } else {
        // Delete auth header
        delete axios.defaults.headers.common['Authorization'];
      }
    };

    export default setAuthToken;

```

And then you can use `setAuthToken` function to set toket to header if user is logged in and delete toket when user
logged out.

For example, if user is successfully logged in via `/customers/login` endpoint you can invoke `setAuthToken(token)`.
Othervise, when token is expired or user logged out by himself - invoke `setAuthToken(false)`;

If you do not want to receive password or other parameters of customer, you can make changes to `getCustomer` controller
function in file `controllers\customers.js` (return to client a set of particular parameters).

```endpoint
GET /customers/customer
```

#### Example request

```javascript
axios
  .get("/customers/customer")
  .then(loggedInCustomer => {
    /*Do something with loggedInCustomer*/
  })
  .catch(err => {
    /*Do something with error */
  });
```

#### Example response

```json
{
  "isAdmin": true,
  "enabled": true,
  "_id": "5d99ce196d40fb1b747bc5f5",
  "firstName": "Customer",
  "lastName": "Newone",
  "login": "Customer",
  "email": "customer@gmail.com",
  "password": "$2a$10$1O/2caDJkzqCYIpBDwMJteUtv.A4PBKFip9YY5WHjZTLwTodxLQcy",
  "telephone": "+380630000000",
  "gender": "male",
  "avatarUrl": "img/customers/023648.png",
  "date": "2019-10-06T11:20:57.544Z",
  "__v": 0
}
```

### Update customer

Endpoint for updating customer is similar to endpoint of creating of new customer. You can pass only those parameters,
that have to be changed.

You can also add your own custom parameters to save additionally in database if you want implemet some custom logic /
functionallity.

```endpoint
PUT /customers
```

#### Example request

```javascript
const updatedCustomer = {
  firstName: "John",
  lastName: "Doe",
  login: "jDoe",
  email: "jDoe@gmail.com",
};

axios
  .put("/customers", updatedCustomer)
  .then(updatedCustomer => {
    /*Do something with customer*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to customer*/
  });
```

#### Example response

```json
{
  "isAdmin": true,
  "enabled": true,
  "_id": "5d99d2532cd5241c40fc9008",
  "firstName": "John",
  "lastName": "Doe",
  "login": "jDoe",
  "email": "jDoe@gmail.com",
  "password": "$2a$10$XTXkPWN3zJufG9S3NATZyeeuoVOwvrzqopfdqYwbUQidCZHp/aTYS",
  "telephone": "+380630000000",
  "gender": "male",
  "avatarUrl": "img/customers/023648.png",
  "customerNo": "27314974",
  "date": "2019-10-06T11:38:59.470Z",
  "__v": 0
}
```

### Change password

Endpoint for changing customers password.

Parameters:

| Property      | Type     | Data type | Description |
| ------------- | -------- | --------- | ----------- |
| `password`    | required | String    | -           |
| `newPassword` | required | String    | -           |

```endpoint
PUT /customers/password
```

#### Example request

```javascript
const passwords = {
  password: "1111111",
  newPassword: "2222222",
};

axios
  .put("/customers/password", passwords)
  .then(updatedCustomer => {
    /*Do something with customer, show info about update*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to customer*/
  });
```

#### Example response

```json
{
  "message": "Password successfully changed",
  "customer": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  }
}
```