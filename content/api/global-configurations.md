
## Global Configurations

In collection "global-configs" you can store different data, that can participate in your store configurations. For
example you can store there different parameters
for storefront, which you want to be able to configure from admin panel, such as "Minimal order value" or the quantity
of products to show on mobile / desktop / tablet versions, enabling or disabling infinity scroll / pagination / lazy
loading etc.

Further you can use this global configurations for connecting different systems and modifying your storefront.

### Add configurations

For adding new configurations use next set of parameters:

| Property      | Type     | Data type | Generated | Description                             |
| ------------- | -------- | --------- | --------- | --------------------------------------- |
| `customId`    | required | String    | by user   | Some unique string                      |
| `date`        | required | Date      | by system | default `Date.now()`                    |

You can also pass any custom parameter that you want.

```
{
  infiniteScrollEnabled: {
    type: Boolean
  },
  paginationEnabled: {
    type: Boolean
  },
  showProductsPerPage: {
    mobile: {
      type: Number
    },
    tablet: {
      type: Number
    },
    desktop: {
      type: Number
    }
  },
  minOrderValue: {
    type: Number
  }
}
```

```endpoint
POST /configs
```

#### Example request

```javascript
const newConfigs = {
  customId: "some-global-configs",
  infiniteScrollEnabled: true,
  minOrderValue: 100,
  someCustomParam: "custom params value",
};

axios
  .post("/configs", newConfigs)
  .then(newConfigs => {
    /*Do something with newConfigs*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "infinitScrollEnabled": true,
  "minOrderValue": 100,
  "someCustomParam": "custom params value"
  "_id": "5dbeaf94235fec2cbc94874b",
  "customId": "some-global-configs",
}
```

### Update configs

This endpoint is for editing data of existing configs by customId. Pass parameters that you wand to add or edit. You
have to pass all nested params to not delete existing ones.

```endpoint
PUT /configs/{customId}
```

#### Example request

```javascript
const updateConfigs = {
  infiniteScrollEnabled: false,
  minOrderValue: 200,
  someCustomParam1: "custom params value 1",
  someCustomParam2: "custom params value 2",
};

axios
  .put("/configs/some-global-configs", updateConfigs)
  .then(updateConfigs => {
    /*Do something with updateConfigs*/
  })
  .cach(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "infinitScrollEnabled": false,
  "minOrderValue": 200,
  "someCustomParam1": "custom params value 1",
  "someCustomParam2": "custom params value 2"
  "_id": "5dbeaf94235fec2cbc94874b",
  "customId": "some-global-configs",
}
```

### Get configs

This endpoint will return all configs. You can also get config by customId GET "/configs/{customId}", e.g. GET "
configs/some-global-configs"

```endpoint
GET /configs
```

#### Example request

```javascript
axios
  .get("/configs")
  .then(configs => {
    /*Do something with configs*/
  })
  .atch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "infinitScrollEnabled": false,
    "minOrderValue": 200,
    "someCustomParam1": "custom params value 1",
    "someCustomParam2": "custom params value 2"
    "_id": "5dbeaf94235fec2cbc94874b",
    "customId": "some-global-configs",
  }
]
```

### Delete configs

This endpoint will delete configs by customId.

```endpoint
GET /configs/{customId}
```

#### Example request

```javascript
axios
  .delete("/configs/some-global-configs")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Config with name \"some-global-configs\" is successfully deleted from DB "
}
```