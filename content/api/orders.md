
## Orders

Order appears when customer add products to cart, go through checkout process and place order.

Endpoints for orders allows you to:

- Place (create) order;
- Update (edit) order;
- Cancel order;
- Delete order;
- Get all orders;
- Get one order by it's id.

### Place (create) order

For creating order use next set of parameters:

| Property          | Type     | Data type | Generated | Description                                              |
| ----------------- | -------- | --------- | --------- | -------------------------------------------------------- |
| `orderNo`         | required | String    | by system | Not pass this param manually                             |
| `customerId`      | optional | ObjectId  | by user   | Pass if customer logged in                               |
| `products`\*      | required | json      | by user   | Array of objects with info about product and quantities  |
| `email`           | required | String    | by user   | -                                                        |
| `mobile`          | required | String    | by user   | -                                                        |
| `letterSubject`   | required | Іекштп    | by user   | Subject for letter for customer                          |
| `letterHtml`      | required | String    | by user   | String with letter body for sending to customer's email  |
| `deliveryAddress` | optional | json      | by user   | Object with data about address where to deliver products |
| `shipping`        | optional | json      | by user   | Object / String with data about shipping                 |
| `paymentInfo`     | optional | json      | by user   | Object / String with data about payment instruments etc. |
| `totalSum`        | required | Number    | by system | Not pass this param manually                             |
| `canceled`        | required | Boolean   | by user   | default: false                                           |
| `status`          | optional | String    | by user   | -                                                        |
| `date`            | required | Date      | by system | default `Date.now()`                                     |

The parameter `products` must have the same structure, as in cart, but the full data about product instead of only
ObjectId.

      "products": [
        {
          "_id": "5dac20058b2cb420e0af4677",
          "product": {
            "enabled": true,
            "imageUrls": [
              "img/products/men/001.png",
              "img/products/men/002.png",
              "img/products/men/003.png",
              "img/products/men/004.png"
            ],
            "quantity": 156,
            "_id": "5da463678cca382250dd7bc7",
            "name": "updted product for testing purposes 222",
            "currentPrice": 100,
            "previousPrice": 250,
            "categories": "men",
            "color": "red",
            "productUrl": "/men",
            "brand": "braaaand",
            "myCustomParam": "some string or json for custom param",
            "itemNo": "291759",
            "date": "2019-10-14T12:00:39.679Z",
            "__v": 0,
            "oneMoreCustomParam": {
              "description": "blablabla",
              "rate": 4.8,
              "likes": 20
            }
          },
          "cartQuantity": 2
        },
        {
          "_id": "5dac20058b2cb420e0af4676",
          "product": {
            "enabled": true,
            "imageUrls": ["products/itemNo2"],
            "quantity": 40,
            "_id": "5d73ad04fcad90130470f08b",
            "name": "test product",
            "currentPrice": 280,
            "categories": "phones",
            "someOtherFeature": "Test feature strict false 2222222222",
            "color": "black",
            "size": "xl",
            "ram": "5",
            "weight": "200g",
            "itemNo": "243965",
            "__v": 0,
            "date": "2019-10-20T08:51:19.287Z"
          },
          "cartQuantity": 3
        }
      ]

If you create order for **non-authenticated customer**, you can go through next steps:

- Get cart from local storage, where you have stored all product IDs and quantities;
- Get these products from DB via GET /products/{itemNo};
- Create expected data structure from all this information: save detail info about every product and it's quantity
  in `products`.

Order is a historycal data, that is why we do not save there only product's ObjectId, but save all details that are
actual at the moment when order is placing.

\*If you are creating order for **authenticated customer**, don't pass `products` to server, it will be taken from
customers cart.

You can also pass your custom parameters if necessary.

![Mail](images/mail.png "Mail")

![Mail](images/mail.png "Mail")

```endpoint
POST /orders
```

#### Example request for logged in customer

```javascript
const newOrder = {
  customerId: "5d99ce196d40fb1b747bc5f5",
  deliveryAddress: {
    country: "Ukraine",
    city: "Kiev",
    address: "Kreshchatic Street 56//A",
    postal: "01044",
  },
  shipping: "Kiev 50UAH",
  paymentInfo: "Credit card",
  status: "not shipped",
  email: "saribeg@gmail.com",
  mobile: "+380630000000",
  letterSubject: "Thank you for order! You are welcome!",
  letterHtml:
    "<h1>Your order is placed. OrderNo is 023689452.</h1><p>{Other details about order in your HTML}</p>",
};

axios
  .post("/orders", newOrder)
  .then(newOrder => {
    /*Do something with newOrder*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response for logged in customer

```json
{
  "order": {
    "products": [
      {
        "_id": "5dac45d43c026f0dac1b3464",
        "product": {
          "enabled": true,
          "imageUrls": [
            "img/products/men/001.png",
            "img/products/men/002.png",
            "img/products/men/003.png",
            "img/products/men/004.png"
          ],
          "quantity": 156,
          "_id": "5da463678cca382250dd7bc7",
          "name": "updted product for testing purposes 222",
          "currentPrice": 100,
          "previousPrice": 250,
          "categories": "men",
          "color": "red",
          "productUrl": "/men",
          "brand": "braaaand",
          "myCustomParam": "some string or json for custom param",
          "itemNo": "291759",
          "date": "2019-10-14T12:00:39.679Z",
          "__v": 0,
          "oneMoreCustomParam": {
            "description": "blablabla",
            "rate": 4.8,
            "likes": 20
          }
        },
        "cartQuantity": 2
      }
    ],
    "canceled": false,
    "_id": "5dac45d53c026f0dac1b3465",
    "customerId": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "deliveryAddress": {
      "country": "Ukraine",
      "city": "Kiev",
      "address": "Kreshchatic Street 56//A",
      "postal": "01044"
    },
    "shipping": "Kiev 50UAH",
    "paymentInfo": "Credit card",
    "status": "not shipped",
    "email": "saribeg@gmail.com",
    "mobile": "+380630000000",
    "letterSubject": "Thank you for order! You are welcome!",
    "letterHtml": "<h1>Your order is placed. OrderNo is 023689452.</h1><p>{Other details about order in your HTML}</p>",
    "orderNo": "7553616",
    "totalSum": 200,
    "date": "2019-10-20T11:32:37.035Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 839,
    "messageTime": 1455,
    "messageSize": 405,
    "response": "250 2.0.0 OK 1571571162 i21sm4981425lfl.44 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<12027d57-6898-f071-0e33-9437d2e6e5aa@gmail.com>"
  }
}
```

#### Example request for non-authenticated customer

```javascript
const newOrder = {
  products: [
    {
      _id: "5dac20058b2cb420e0af4677",
      product: {
        enabled: true,
        imageUrls: [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png",
        ],
        quantity: 156,
        _id: "5da463678cca382250dd7bc7",
        name: "updted product for testing purposes 222",
        currentPrice: 100,
        previousPrice: 250,
        categories: "men",
        color: "red",
        productUrl: "/men",
        brand: "braaaand",
        myCustomParam: "some string or json for custom param",
        itemNo: "291759",
        date: "2019-10-14T12:00:39.679Z",
        __v: 0,
        oneMoreCustomParam: {
          description: "blablabla",
          rate: 4.8,
          likes: 20,
        },
      },
      cartQuantity: 2,
    },
    {
      _id: "5dac20058b2cb420e0af4676",
      product: {
        enabled: true,
        imageUrls: ["products/itemNo2"],
        quantity: 40,
        _id: "5d73ad04fcad90130470f08b",
        name: "test product",
        currentPrice: 280,
        categories: "phones",
        someOtherFeature: "Test feature strict false 2222222222",
        color: "black",
        size: "xl",
        ram: "5",
        weight: "200g",
        itemNo: "243965",
        __v: 0,
        date: "2019-10-20T08:51:19.287Z",
      },
      cartQuantity: 3,
    },
  ],
  deliveryAddress: {
    country: "Ukraine",
    city: "Kiev",
    address: "Kreshchatic Street 56//A",
    postal: "01044",
  },
  shipping: "Kiev 50UAH",
  paymentInfo: "Credit card",
  status: "not shipped",
  email: "saribeg@gmail.com",
  mobile: "+380630000000",
  letterSubject: "Thank you for order! You are welcome!",
  letterHtml:
    "<h1>Your order is placed. OrderNo is 023689452.</h1><p>{Other details about order in your HTML}</p>",
};

axios
  .post("/orders", newOrder)
  .then(newOrder => {
    /*Do something with newOrder*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response for non-authenticated customer

```json
{
  "order": {
    "products": [
      {
        "_id": "5dac20058b2cb420e0af4677",
        "product": {
          "enabled": true,
          "imageUrls": [
            "img/products/men/001.png",
            "img/products/men/002.png",
            "img/products/men/003.png",
            "img/products/men/004.png"
          ],
          "quantity": 156,
          "_id": "5da463678cca382250dd7bc7",
          "name": "updted product for testing purposes 222",
          "currentPrice": 100,
          "previousPrice": 250,
          "categories": "men",
          "color": "red",
          "productUrl": "/men",
          "brand": "braaaand",
          "myCustomParam": "some string or json for custom param",
          "itemNo": "291759",
          "date": "2019-10-14T12:00:39.679Z",
          "__v": 0,
          "oneMoreCustomParam": {
            "description": "blablabla",
            "rate": 4.8,
            "likes": 20
          }
        },
        "cartQuantity": 2
      },
      {
        "_id": "5dac20058b2cb420e0af4676",
        "product": {
          "enabled": true,
          "imageUrls": ["products/itemNo2"],
          "quantity": 40,
          "_id": "5d73ad04fcad90130470f08b",
          "name": "test product",
          "currentPrice": 280,
          "categories": "phones",
          "someOtherFeature": "Test feature strict false 2222222222",
          "color": "black",
          "size": "xl",
          "ram": "5",
          "weight": "200g",
          "itemNo": "243965",
          "__v": 0,
          "date": "2019-10-20T08:51:19.287Z"
        },
        "cartQuantity": 3
      }
    ],
    "canceled": false,
    "_id": "5dac48fccc07d50acc8c5eab",
    "deliveryAddress": {
      "country": "Ukraine",
      "city": "Kiev",
      "address": "Kreshchatic Street 56//A",
      "postal": "01044"
    },
    "shipping": "Kiev 50UAH",
    "paymentInfo": "Credit card",
    "status": "not shipped",
    "email": "saribeg@gmail.com",
    "mobile": "+380630000000",
    "letterSubject": "Thank you for order! You are welcome!",
    "letterHtml": "<h1>Your order is placed. OrderNo is 023689452.</h1><p>{Other details about order in your HTML}</p>",
    "orderNo": "6991117",
    "totalSum": 1040,
    "date": "2019-10-20T11:46:04.103Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 538,
    "messageTime": 1439,
    "messageSize": 405,
    "response": "250 2.0.0 OK 1571571968 p22sm5888527ljp.69 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<fa72de47-686e-2ea9-5810-383917a0ef44@gmail.com>"
  }
}
```

### Update order

This endpoint is for editing data of existing order if necessary. Pass parameters that you wand to add or edit.

```endpoint
PUT /orders/{id}
```

#### Example request

```javascript
const updatedOrder = {
  status: "shipped",
};

axios
  .put("/orders/5dac45d53c026f0dac1b3465", updatedOrder)
  .then(updatedOrder => {
    /*Do something with updatedOrder*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "order": {
    "products": [
      {
        "_id": "5dac45d43c026f0dac1b3464",
        "product": {
          "enabled": true,
          "imageUrls": [
            "img/products/men/001.png",
            "img/products/men/002.png",
            "img/products/men/003.png",
            "img/products/men/004.png"
          ],
          "quantity": 156,
          "_id": "5da463678cca382250dd7bc7",
          "name": "updted product for testing purposes 222",
          "currentPrice": 100,
          "previousPrice": 250,
          "categories": "men",
          "color": "red",
          "productUrl": "/men",
          "brand": "braaaand",
          "myCustomParam": "some string or json for custom param",
          "itemNo": "291759",
          "date": "2019-10-14T12:00:39.679Z",
          "__v": 0,
          "oneMoreCustomParam": {
            "description": "blablabla",
            "rate": 4.8,
            "likes": 20
          }
        },
        "cartQuantity": 2
      }
    ],
    "canceled": false,
    "_id": "5dac45d53c026f0dac1b3465",
    "customerId": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "deliveryAddress": {
      "country": "Ukraine",
      "city": "Kiev",
      "address": "Kreshchatic Street 56//A",
      "postal": "01044"
    },
    "shipping": "Kiev 50UAH",
    "paymentInfo": "Credit card",
    "status": "shipped",
    "email": "saribeg@gmail.com",
    "mobile": "+380630000000",
    "letterSubject": "Thank you for order! You are welcome!",
    "letterHtml": "sfsdfsdfsdfsdf",
    "orderNo": "7553616",
    "totalSum": 200,
    "date": "2019-10-20T11:32:37.035Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 227,
    "messageTime": 1942,
    "messageSize": 305,
    "response": "250 2.0.0 OK 1571572819 a28sm3886291lfk.29 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<d8a2c110-4cbb-e689-ece4-651ef085344c@gmail.com>"
  }
}
```

### Cancel order

It only turns parameter `canceled` to true and send mail to customer.

```endpoint
PUT /orders/cancel/{id}
```

#### Example request

```javascript
axios
  .put("/orders/cancel/5dac45d53c026f0dac1b3465")
  .then(order => {
    /*Do something with order*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "order": {
    "products": [
      {
        "_id": "5dac20058b2cb420e0af4677",
        "product": {
          "enabled": true,
          "imageUrls": [
            "img/products/men/001.png",
            "img/products/men/002.png",
            "img/products/men/003.png",
            "img/products/men/004.png"
          ],
          "quantity": 156,
          "_id": "5da463678cca382250dd7bc7",
          "name": "updted product for testing purposes 222",
          "currentPrice": 100,
          "previousPrice": 250,
          "categories": "men",
          "color": "red",
          "productUrl": "/men",
          "brand": "braaaand",
          "myCustomParam": "some string or json for custom param",
          "itemNo": "291759",
          "date": "2019-10-14T12:00:39.679Z",
          "__v": 0,
          "oneMoreCustomParam": {
            "description": "blablabla",
            "rate": 4.8,
            "likes": 20
          }
        },
        "cartQuantity": 33
      },
      {
        "_id": "5dac20058b2cb420e0af4676",
        "product": {
          "enabled": true,
          "imageUrls": ["products/itemNo2"],
          "quantity": 40,
          "_id": "5d73ad04fcad90130470f08b",
          "name": "test product",
          "currentPrice": 280,
          "categories": "phones",
          "someOtherFeature": "Test feature strict false 2222222222",
          "color": "black",
          "size": "xl",
          "ram": "5",
          "weight": "200g",
          "itemNo": "243965",
          "__v": 0,
          "date": "2019-10-20T08:51:19.287Z"
        },
        "cartQuantity": 3
      }
    ],
    "canceled": true,
    "_id": "5dac45d53c026f0dac1b3465",
    "customerId": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "deliveryAddress": {
      "country": "Ukraine",
      "city": "Kiev",
      "address": "Kreshchatic Street 56//A",
      "postal": "01044"
    },
    "shipping": "Kiev 50UAH",
    "paymentInfo": "Credit card",
    "status": "shipped",
    "email": "saribeg@gmail.com",
    "mobile": "+380630000000",
    "letterSubject": "Thank you for order! You are welcome!",
    "letterHtml": "sfsdfsdfsdfsdf",
    "orderNo": "7553616",
    "totalSum": 4140,
    "date": "2019-10-20T11:32:37.035Z",
    "__v": 0
  },
  "mailResult": {
    "accepted": ["saribeg@gmail.com"],
    "rejected": [],
    "envelopeTime": 170,
    "messageTime": 655,
    "messageSize": 305,
    "response": "250 2.0.0 OK 1571573129 c76sm7234957lfg.11 - gsmtp",
    "envelope": {
      "from": "2019.matter.store@gmail.com",
      "to": ["saribeg@gmail.com"]
    },
    "messageId": "<6a981463-9435-0345-4d08-d60678f92724@gmail.com>"
  }
}
```

### Delete order

```endpoint
DELETE /orders/{id}
```

#### Example request

```javascript
axios
  .delete("/orders/5dac45d53c026f0dac1b3465")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Order with id \"5dac45d53c026f0dac1b3465\" is successfully deletes from DB. Order Details: { products: \n [ { _id: '5dac20058b2cb420e0af4677',\n product: [Object],\n cartQuantity: 33 },\n { _id: '5dac20058b2cb420e0af4676',\n product: [Object],\n cartQuantity: 3 } ],\n canceled: true,\n _id: 5dac45d53c026f0dac1b3465,\n customerId: 5d99ce196d40fb1b747bc5f5,\n deliveryAddress: \n { country: 'Ukraine',\n city: 'Kiev',\n address: 'Kreshchatic Street 56//A',\n postal: '01044' },\n shipping: 'Kiev 50UAH',\n paymentInfo: 'Credit card',\n status: 'shipped',\n email: 'saribeg@gmail.com',\n mobile: '+380630000000',\n letterSubject: 'Thank you for order! You are welcome!',\n letterHtml: 'sfsdfsdfsdfsdf',\n orderNo: '7553616',\n totalSum: 4140,\n date: 2019-10-20T11:32:37.035Z,\n __v: 0 }"
}
```

### Get customer orders

Get orders of logged in customer. This requires authorization

```endpoint
GET /orders
```

#### Example request

```javascript
axios
  .get("/orders")
  .then(orders => {
    /*Do something with orders*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
[
  {
    "products": [
      {
        "_id": "5dac4ecd6bbe0e05e487f0be",
        "product": {
          "enabled": true,
          "imageUrls": [
            "img/products/men/001.png",
            "img/products/men/002.png",
            "img/products/men/003.png",
            "img/products/men/004.png"
          ],
          "quantity": 156,
          "_id": "5da463678cca382250dd7bc7",
          "name": "updted product for testing purposes 222",
          "currentPrice": 100,
          "previousPrice": 250,
          "categories": "men",
          "color": "red",
          "productUrl": "/men",
          "brand": "braaaand",
          "myCustomParam": "some string or json for custom param",
          "itemNo": "291759",
          "date": "2019-10-14T12:00:39.679Z",
          "__v": 0,
          "oneMoreCustomParam": {
            "description": "blablabla",
            "rate": 4.8,
            "likes": 20
          }
        },
        "cartQuantity": 2
      }
    ],
    "canceled": false,
    "_id": "5dac4ece6bbe0e05e487f0bf",
    "deliveryAddress": {
      "country": "Ukraine",
      "city": "Kiev",
      "address": "Kreshchatic Street 56//A",
      "postal": "01044"
    },
    "letterSubject": "Thank you for order! You are welcome!",
    "email": "saribeg@gmail.com",
    "letterHtml": "sfsdfsdfsdfsdf",
    "mobile": "+38063000000",
    "status": "shipped",
    "customerId": {
      "isAdmin": true,
      "enabled": true,
      "_id": "5d99ce196d40fb1b747bc5f5",
      "firstName": "Customer",
      "lastName": "Newone",
      "login": "Customer",
      "email": "customer@gmail.com",
      "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
      "telephone": "+380630000000",
      "gender": "male",
      "avatarUrl": "img/customers/023648.png",
      "date": "2019-10-06T11:20:57.544Z",
      "__v": 0
    },
    "orderNo": "8859595",
    "totalSum": 200,
    "date": "2019-10-20T12:10:54.113Z",
    "__v": 0
  }
]
```

### Get order by orderNo

```endpoint
GET /orders/{orderNo}
```

#### Example request

```javascript
axios
  .get("/orders/6991117")
  .then(order => {
    /*Do something with order*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "products": [
    {
      "_id": "5dac20058b2cb420e0af4677",
      "product": {
        "enabled": true,
        "imageUrls": [
          "img/products/men/001.png",
          "img/products/men/002.png",
          "img/products/men/003.png",
          "img/products/men/004.png"
        ],
        "quantity": 156,
        "_id": "5da463678cca382250dd7bc7",
        "name": "updted product for testing purposes 222",
        "currentPrice": 100,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "291759",
        "date": "2019-10-14T12:00:39.679Z",
        "__v": 0,
        "oneMoreCustomParam": {
          "description": "blablabla",
          "rate": 4.8,
          "likes": 20
        }
      },
      "cartQuantity": 2
    },
    {
      "_id": "5dac20058b2cb420e0af4676",
      "product": {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad04fcad90130470f08b",
        "name": "test product",
        "currentPrice": 280,
        "categories": "phones",
        "someOtherFeature": "Test feature strict false 2222222222",
        "color": "black",
        "size": "xl",
        "ram": "5",
        "weight": "200g",
        "itemNo": "243965",
        "__v": 0,
        "date": "2019-10-20T08:51:19.287Z"
      },
      "cartQuantity": 3
    }
  ],
  "canceled": false,
  "_id": "5dac48fccc07d50acc8c5eab",
  "deliveryAddress": {
    "country": "Ukraine",
    "city": "Kiev",
    "address": "Kreshchatic Street 56//A",
    "postal": "01044"
  },
  "shipping": "Kiev 50UAH",
  "paymentInfo": "Credit card",
  "status": "not shipped",
  "email": "saribeg@gmail.com",
  "mobile": "+380630000000",
  "letterSubject": "Thank you for order! You are welcome!",
  "letterHtml": "<h1>Your order is placed. OrderNo is 023689452.</h1><p>{Other details about order in your HTML}</p>",
  "orderNo": "6991117",
  "totalSum": 1040,
  "date": "2019-10-20T11:46:04.103Z",
  "__v": 0
}
```
