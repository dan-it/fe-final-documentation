## Products

You can manage products in your store.

We have endpoints to:

- Add new product;
- Get all products;
- Get specific product;
- Edit specific product;
- Get products by search phrases.
- Upload images for specific product;

### Add new product

For adding new product use next set of parameters:

| Property              | Type     | Data type | Generated | Description                                       |
| --------------------- | -------- | --------- | --------- | ------------------------------------------------- |
| `enabled`             | required | Boolean   | by user   | Default value - `true`                            |
| `name`                | required | String    | by user   | Name of product to show on store interface        |
| `currentPrice`        | required | Number    | by user   | Price to buy product                              |
| `previousPrice`       | optional | Number    | by user   | Сrossed out proce (if promotion)                  |
| `categories`          | required | String    | by user   | A low level category to which product is assigned |
| `imageUrls`           | required | json      | by user   | Array of strings with url paths to product images |
| `quantity`            | required | Number    | by user   | Quantity of product in store. efault = 0          |
| `color`               | optional | String    | by user   | -                                                 |
| `sizes`               | optional | String    | by user   | -                                                 |
| `productUrl`          | optional | String    | by user   | -                                                 |
| `brand`               | optional | String    | by user   | -                                                 |
| `manufacturer`        | optional | String    | by user   | -                                                 |
| `manufacturerCountry` | optional | String    | by user   | -                                                 |
| `seller`              | optional | String    | by user   | -                                                 |
| `date`                | required | Date      | by system | default `Date.now()`                              |

You can also pass your custom parameters.
Custom parameter can be either string or json (for arrays / objects).

```endpoint
POST /products
```

#### Example request

```javascript
const newProduct = {
  name: "new product for testing purposes",
  currentPrice: 199.99,
  previousPrice: 250,
  categories: "men",
  imageUrls: [
    "img/products/men/001.png",
    "img/products/men/002.png",
    "img/products/men/003.png",
    "img/products/men/004.png",
  ],
  quantity: 100,
  color: "red",
  productUrl: "/men",
  brand: "braaaand",
  myCustomParam: "some string or json for custom param",
};

axios
  .post("/products", newProduct)
  .then(newProduct => {
    /*Do something with newProduct*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "imageUrls": [
    "img/products/men/001.png",
    "img/products/men/002.png",
    "img/products/men/003.png",
    "img/products/men/004.png"
  ],
  "quantity": 100,
  "_id": "5da463678cca382250dd7bc7",
  "name": "new product for testing purposes",
  "currentPrice": 199.99,
  "previousPrice": 250,
  "categories": "men",
  "color": "red",
  "productUrl": "/men",
  "brand": "braaaand",
  "myCustomParam": "some string or json for custom param",
  "itemNo": "291759",
  "date": "2019-10-14T12:00:39.679Z",
  "__v": 0
}
```



### Get all products

This endpoint is for getting products.

**Filter products**

It is possible to get filtered products by query parameters:

`/products?param1=value1&param2=value2-1,value2-2,value2-3&param3=value3&perPage=2&startPage=1`

As you can see, query string can contain different parameters and values.
For example `/products?color=red,blue,black&size=xl&categories=phones&perPage=2&startPage=1` means "Give me products, where":
- color can be red, blue or black;
- size can be xl;
- category is phones;
- give me only 2 products per page (even if there are 10 products, you will receive 2);
- start from page 1 (you can receive products by portion via infinity sroll, passing different `perPage`
  and `startPage`).

**Sort products**

You can also pass param for sorting by one key, e.g. `-price`
or `+price`: `/products?param1=value1&param2=value2-1,value2-2,value2-3&param3=value3&perPage=2&startPage=1&sort=-price`

Of course, params have to be exactly as in product objects (match) as there will be executed search in DB for theese
products by there params names.

**Search products**

It is possible to retrieve a list of products with the option to filter by product name. The search functionality is facilitated through the "q" query parameter, which accepts a string query for product name matching.

`/api/products?q=iphone`


```endpoint
GET /products
```

#### Example request

```javascript
axios
  .get("/products")
  .then(products => {
    /*Do something with products*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "data": [
    {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad04fcad90130470f08b",
        "name": "test product",
        "currentPrice": 280,
        "categories": "phones",
        "someOtherFeature": "Test feature strict false 2222222222",
        "color": "black",
        "size": "xl",
        "ram": "5",
        "weight": "200g",
        "itemNo": "243965",
        "__v": 0,
        "date": "2019-10-14T12:46:28.041Z"
    },
    {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad2bfcad90130470f08c",
        "name": "test product 2",
        "currentPrice": 35,
        "categories": "phones",
        "someOtherFeature": "test2",
        "color": "white",
        "size": "x",
        "ram": "23",
        "weight": "100g",
        "itemNo": "341527",
        "__v": 0,
        "date": "2019-10-14T12:46:28.041Z"
    },
    {
        "enabled": true,
        "imageUrls": ["products/itemNo2"],
        "quantity": 40,
        "_id": "5d73ad7cfcad90130470f08d",
        "name": "test product 3",
        "currentPrice": 100,
        "categories": "notebooks",
        "someOtherFeature": "test3",
        "color": "red",
        "size": "l",
        "ram": "50",
        "weight": "4kg",
        "itemNo": "831009",
        "__v": 0,
        "date": "2019-10-14T12:46:28.042Z"
    }
  ],
  "total": 3
}
```

#### Example request

```javascript
axios
  .get("/products?color=red,blue,black&size=xl&categories=phones")
  .then(products => {
    /*Do something with products*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });

axios
  .get("/products?size=xl&perPage=2&startPage=1")
  .then(products => {
    /*Do something with products*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "data": [
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-14T12:58:23.638Z"
    }
  ],
  "total": 1
}
```

```json
{
  "data": [
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-14T13:01:16.284Z"
    },
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad9afcad90130470f08e",
      "name": "test product 4",
      "currentPrice": 333,
      "categories": "notebooks",
      "someOtherFeature": "test4",
      "color": "red",
      "size": "xl",
      "ram": "600",
      "weight": "4kg",
      "itemNo": "450717",
      "__v": 0,
      "date": "2019-10-14T13:01:16.285Z"
    }
  ],
  "total": 2
}
```

```endpoint
GET /api/products?q=iphone
```

#### Example request

```javascript
axios
  .get("/products?q=iphone")
  .then(products => {
    /*Do something with products*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "data": [
    {
      "_id": "5d73ad04fcad90130470f08b",
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "name": "iPhone 15 Pro",
      "currentPrice": 2800,
      "categories": "phones",
      "color": "black",
      "itemNo": "243965",
      "date": "2019-10-14T13:01:16.284Z"
    },
    {
      "_id": "64d271469a423b25869927fc",
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "name": "iPhone 15 Pro Max",
      "currentPrice": 3200,
      "categories": "phones",
      "color": "black",
      "itemNo": "243965",
      "date": "2019-10-14T13:01:16.284Z"
    },
  ]
  "total": 2
}

```

### Get one product

This endpoint is for getting one specific product by it's `id`.

```endpoint
GET /products/{id}
```

#### Example request

```javascript
axios
  .get("/products/64d2743886226e263f3c34ec")
  .then(product => {
    /*Do something with product*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "imageUrls": [
    "img/products/men/001.png",
    "img/products/men/002.png",
    "img/products/men/003.png",
    "img/products/men/004.png"
  ],
  "quantity": 156,
  "_id": "64d2743886226e263f3c34ec",
  "name": "updated product for testing purposes 222",
  "currentPrice": 100,
  "previousPrice": 250,
  "categories": "men",
  "color": "red",
  "productUrl": "/men",
  "brand": "braaaand",
  "myCustomParam": "some string or json for custom param",
  "itemNo": "291759",
  "date": "2019-10-14T12:00:39.679Z",
  "__v": 0,
  "oneMoreCustomParam": {
    "description": "blablabla",
    "rate": 4.8,
    "likes": 20
  }
}
```

### Update product

This endpoint is for editing data of existing product. For finding particular product for editing it's `_id` (ObjectId)is
used. Pass parameters that you wand to add or edit.

```endpoint
PUT /products/{id}
```

#### Example request

```javascript
const updatedProduct = {
  name: "Updted product for testing purposes 222",
  quantity: 156,
  currentPrice: 100,
  brand: "new brand",
  oneMoreCustomParam: `{"description": "blablabla", "rate": 4.8, "likes": 20}`,
};

axios
  .put("/products/5da463678cca382250dd7bc7", updatedProduct)
  .then(updatedProduct => {
    /*Do something with updatedProduct*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "enabled": true,
  "imageUrls": [
    "img/products/men/001.png",
    "img/products/men/002.png",
    "img/products/men/003.png",
    "img/products/men/004.png"
  ],
  "quantity": 156,
  "_id": "5da463678cca382250dd7bc7",
  "name": "updted product for testing purposes 222",
  "currentPrice": 100,
  "previousPrice": 250,
  "categories": "men",
  "color": "red",
  "productUrl": "/men",
  "brand": "braaaand",
  "myCustomParam": "some string or json for custom param",
  "itemNo": "291759",
  "date": "2019-10-14T12:00:39.679Z",
  "__v": 0,
  "oneMoreCustomParam": {
    "description": "blablabla",
    "rate": 4.8,
    "likes": 20
  }
}
```

### Add images

This endpoint is for adding images.

```endpoint
POST /products/images
```

#### Example request

```javascript
axios
  .post("/products/images", formData, {
    headers: {
      path: `./static/images/products/`,
      "content-type": "multipart/form-data",
    },
  })
  .then(response => {
    /*Do something with response*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Photos are received"
}
```