
## Wishlist

Wishlist functionality have to be available only for authorized customers.
The data structure and functionality looks like Cart, but instead cart customers add products to their wishlist.

Endpoints for wishlist allows you to:

- Create wishlist (only 1 instance). Customer can not have more than one wishlist;
- Update (edit) wishlist. If wishlist is not created yet, this will create wishlist;
- Add product to wishlist;
- Delete wishlist;
- Delete product from wishlist;
- Get wishlist.

### Create wishlist

For creating new wishlist use next set of parameters:

| Property     | Type     | Data type | Generated | Description                                         |
| ------------ | -------- | --------- | --------- | --------------------------------------------------- |
| `customerId` | required | ObjectId  | by system | Not pass this param manually                        |
| `products`   | optional | json      | by user   | Array of products' ObjectId with info about product |
| `date`       | required | Date      | by system | default `Date.now()`                                |

The parameter `products` should have the next structure:

      "products": ["5da463678cca382250dd7bc7", "5d73ad04fcad90130470f08b", "5d73ad2bfcad90130470f08c"]

You can also pass your custom parameters if necessary.

```endpoint
POST /wishlist
```

#### Example request

```javascript
const newWishlist = {
  products: ["5da463678cca382250dd7bc7", "5d73ad04fcad90130470f08b"]
};

axios
  .post("/wishlist", newWishlist)
  .then(newWishlist => {
    /_Do something with newWishlist_/
  })
  .catch(err => {
    /_Do something with error, e.g. show error to user_/
  });
```

#### Example response

```json
{
  "products": [
    {
      "enabled": true,
      "imageUrls": [
        "img/products/men/001.png",
        "img/products/men/002.png",
        "img/products/men/003.png",
        "img/products/men/004.png"
      ],
      "quantity": 156,
      "_id": "5da463678cca382250dd7bc7",
      "name": "updted product for testing purposes 222",
      "currentPrice": 100,
      "previousPrice": 250,
      "categories": "men",
      "color": "red",
      "productUrl": "/men",
      "brand": "braaaand",
      "myCustomParam": "some string or json for custom param",
      "itemNo": "291759",
      "date": "2019-10-14T12:00:39.679Z",
      "__v": 0,
      "oneMoreCustomParam": {
        "description": "blablabla",
        "rate": 4.8,
        "likes": 20
      }
    },
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-22T16:19:27.117Z"
    }
  ],
  "_id": "5daf2c0d7f3a57225409f903",
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-22T16:19:25.966Z",
  "__v": 0
}
```

### Update wishlist

This endpoint is for editing data of existing wishlist. Pass parameters that you wand to add or edit.

```endpoint
PUT /wishlist
```

#### Example request

```javascript
const updatedWishlist = {
  products: ["5da463678cca382250dd7bc7"],
  someCustomParam: "The value of custom param",
};

axios
  .put("/wishlist", updatedWishlist)
  .then(updatedWishlist => {
    /*Do something with updatedCart*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "products": [
    {
      "enabled": true,
      "imageUrls": [
        "img/products/men/001.png",
        "img/products/men/002.png",
        "img/products/men/003.png",
        "img/products/men/004.png"
      ],
      "quantity": 156,
      "_id": "5da463678cca382250dd7bc7",
      "name": "updted product for testing purposes 222",
      "currentPrice": 100,
      "previousPrice": 250,
      "categories": "men",
      "color": "red",
      "productUrl": "/men",
      "brand": "braaaand",
      "myCustomParam": "some string or json for custom param",
      "itemNo": "291759",
      "date": "2019-10-14T12:00:39.679Z",
      "__v": 0,
      "oneMoreCustomParam": {
        "description": "blablabla",
        "rate": 4.8,
        "likes": 20
      }
    }
  ],
  "_id": "5daf2c0d7f3a57225409f903",
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-22T16:19:25.966Z",
  "__v": 0,
  "someCustomParam": "The value of custom param"
}
```

### Add product to wishlist

Add one product to wishlist. If wishlist does not exists, it will be created and product will be added to it. If
wishlist already exists, product will be added to existing arrays of products.

```endpoint
PUT /wishlist/{productId}
```

#### Example request

```javascript
axios
  .put("/wishlist/5d73ad04fcad90130470f08b")
  .then(updatedWishlist => {
    /*Do something with updatedWishlist*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "products": [
    {
      "enabled": true,
      "imageUrls": [
        "img/products/men/001.png",
        "img/products/men/002.png",
        "img/products/men/003.png",
        "img/products/men/004.png"
      ],
      "quantity": 156,
      "_id": "5da463678cca382250dd7bc7",
      "name": "updted product for testing purposes 222",
      "currentPrice": 100,
      "previousPrice": 250,
      "categories": "men",
      "color": "red",
      "productUrl": "/men",
      "brand": "braaaand",
      "myCustomParam": "some string or json for custom param",
      "itemNo": "291759",
      "date": "2019-10-14T12:00:39.679Z",
      "__v": 0,
      "oneMoreCustomParam": {
        "description": "blablabla",
        "rate": 4.8,
        "likes": 20
      }
    },
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-22T16:23:03.518Z"
    }
  ],
  "_id": "5daf2c0d7f3a57225409f903",
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-22T16:19:25.966Z",
  "__v": 0,
  "someCustomParam": "The value of custom param"
}
```

### Delete product from wishlist

This endpoint is for deleting one product from wishlist.

```endpoint
DELETE /wishlist/{productId}
```

#### Example request

```javascript
axios
  .delete("/wishlist/5da463678cca382250dd7bc7")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "products": [
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-22T16:24:18.376Z"
    }
  ],
  "_id": "5daf2c0d7f3a57225409f903",
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-22T16:19:25.966Z",
  "__v": 0,
  "someCustomParam": "The value of custom param"
}
```

### Get wishlist

This endpoint will return wishlist with detail information about customer and all products, added to wishlist.

```endpoint
GET /wishlist
```

#### Example request

```javascript
axios
  .get("/wishlist")
  .then(wishlist => {
    /*Do something with wishlist*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "products": [
    {
      "enabled": true,
      "imageUrls": ["products/itemNo2"],
      "quantity": 40,
      "_id": "5d73ad04fcad90130470f08b",
      "name": "test product",
      "currentPrice": 280,
      "categories": "phones",
      "someOtherFeature": "Test feature strict false 2222222222",
      "color": "black",
      "size": "xl",
      "ram": "5",
      "weight": "200g",
      "itemNo": "243965",
      "__v": 0,
      "date": "2019-10-22T16:24:56.209Z"
    }
  ],
  "_id": "5daf2c0d7f3a57225409f903",
  "customerId": {
    "isAdmin": true,
    "enabled": true,
    "_id": "5d99ce196d40fb1b747bc5f5",
    "firstName": "Customer",
    "lastName": "Newone",
    "login": "Customer",
    "email": "customer@gmail.com",
    "password": "$2a$10$c4aXjoQfb5X3e4a2wZ.L0.mv0V/a3kXbV7oW6FxanCQDYcOOId10m",
    "telephone": "+380630000000",
    "gender": "male",
    "avatarUrl": "img/customers/023648.png",
    "date": "2019-10-06T11:20:57.544Z",
    "__v": 0
  },
  "date": "2019-10-22T16:19:25.966Z",
  "__v": 0,
  "someCustomParam": "The value of custom param"
}
```

### Delete wishlist

This endpoint is for deleting the whole wishlist.

```endpoint
DELETE /wishlist
```

#### Example request

```javascript
axios
  .delete("/wishlist")
  .then(result => {
    /*Do something with result*/
  })
  .catch(err => {
    /*Do something with error, e.g. show error to user*/
  });
```

#### Example response

```json
{
  "message": "Wishlist witn id \"5daf2c0d7f3a57225409f903\" is successfully deletes from DB "
}
```