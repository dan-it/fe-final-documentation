## About Project

Welcome to **[DAN.IT Education](https://dan-it.com.ua/)**.

This documentation is about Node.js API for Front End course to use in Final Project for creating ECommerce-Store.

Here you will find everything about project basic configurations and about how to use endpoints for managing content of your online-store.

Thus, you will have the opportunity to concentrate on implementing the front-end part and use the ready-made backend code.

If you don't have knowledge about RESTful API, please, read these articles:

*   [REST: простым языком](https://medium.com/@andr.ivas12/rest-%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%8B%D0%BC-%D1%8F%D0%B7%D1%8B%D0%BA%D0%BE%D0%BC-90a0bca0bc78)
*   [REST API Best Practices](https://habr.com/ru/post/351890/)
*   [Как правильно работать с REST API](https://itvdn.com/ru/blog/article/rest-api-18)

Lets start.

## Preparations

Every team have to create their own git repository for Final Project and copy to it ready-made backend code. To do this, you (one person from team) can go through the following steps:

1.  Create your repository on [Github](https://github.com/)

2.  Clone it to any convenient folder, where you will work with project.

3.  Download and unzip backend part from [Gitlab repository](https://gitlab.com/dan-it/fe-final-backend)

4.  Copy all nested folders and files to your project so that `package.json` file from backend will be in project root folder.

5.  Install dependencies in your project folder: `npm install`

    You will see something like this: ![File structure](images/file_structure.png "File structure")

6.  Create within your project's root folder a new folder named `client`. In this folder you should write all frontend code.

7.  Push all changes (file structure) to git.

## Configurations

At backend side you have some personal accounts to access different systems. For this reason you must (one person from project) create needed accounts and configure credentials in appropriate files according to steps below:

1. Create account for accessing MongoDB at [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)

2. Create there your Database.

3. Go to your project and duplicate `.env.example` file renamed to `.env`. 
This file is under `.gitignore` so every team member should do this

4. Configure DB Credentials in backend files
    - `MONGO_URI`` in `.env`
5.  Create or use existing gmail-account for your online-store mailing.
This will be used for sending to customers's email information about placed orders and subscribes.

7.  Configure credentials for mailing in `.env` file. 

That is all.

If ure are familiar with Node.js - you can modify code if it will be necessary during Final Project development
