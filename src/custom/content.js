var fs = require('fs');

/**
 * This file exports the content of your website, as a bunch of concatenated
 * Markdown files. By doing this explicitly, you can control the order
 * of content without any level of abstraction.
 *
 * Using the brfs module, fs.readFileSync calls in this file are translated
 * into strings of those files' content before the file is delivered to a
 * browser: the content is read ahead-of-time and included in bundle.js.
 */
module.exports =
  '# Basic configurations\n' +
  fs.readFileSync('./content/basic.md', 'utf8') + '\n' +
  '# Our API\n' +
  fs.readFileSync('./content/api/customers.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/products.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/cart.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/orders.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/wishlist.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/catalog.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api/global-configurations.md', 'utf8') + '\n' +
  fs.readFileSync('./content/api.md', 'utf8') + '\n';
